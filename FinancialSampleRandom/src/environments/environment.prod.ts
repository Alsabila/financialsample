export const environment = {
    production: true,
    rootApi: 'http://34.101.241.250/',
    rootStaticApi: 'http://34.101.241.250/',
    role: {
        'administrator': ['ichlasul.affan@gmail.com', 'ilhamperuzzi@gmail.com'],
        'staff': ['ichlasul.affan@gmail.com'],
        'donor': ['ichlasul.affan@gmail.com'],
        'registered': ['ichlasul.affan@gmail.com']
    }

};