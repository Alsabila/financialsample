import {
    Requirement,
    NextFeature
} from "../assessment-object/AssessmentObject";
import {
    Assessment
} from "./Assessment";

export class AssessmentOne extends Assessment {
    private req1 = new Requirement("Berhasil membuat program", 1, 0);
    private nextF = new NextFeature("Catatan Pemasukan", "Menyajikan catatan pemasukan dana untuk situs Anda", require('../../../assets/images/income.png'));

    constructor() {
        super();
        this.updateData("All", 0);
        this.setRequirements([this.req1]);
        this.setNextFetures(this.nextF);
    }

    modifyNextFeature(nextFeatureName: string) {
        console.log("Already constructed")
    }

    getProgramCount() {
        fetch('http://34.101.241.250/api/program/list.abs').then(res => res.json())
            .then(data => this.req1.setCurrentTotal(data['data'].length))
            .catch(error => console.log("Error"));
    }

    updateData(name: string, count: number) {
        if (name == "All") {
            this.getProgramCount();
        } else if (name == "Program") {
            this.req1.setCurrentTotal(count);
        }
    }

    getStatus() {
        if (this.req1.getCurrentTotal() >= this.req1.getTotal()) {
            return "Achieved";
        } else {
            return "Not Achieved";
        }
    }

    getAssessmentName() {
        return "program";
    }
}