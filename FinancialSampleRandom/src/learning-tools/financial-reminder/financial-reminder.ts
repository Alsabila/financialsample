import Swal from 'sweetalert2';

export class FinancialReminder {
    private currentDate: Date;

    constructor() {}

    public getCurrentDateFromLocalStorage() {
        return localStorage.getItem("Current Date");
    }

    public setCurrentDateInLocalStorage(date: string) {
        localStorage.setItem("Current Date", date);
    }

    public showReminderAtFour() {
        this.currentDate = new Date();
        if (this.currentDate.getDate().toString() != localStorage.getItem("Current Date")) {
            Swal.fire({
                title: 'Jangan Lupa!',
                text: 'Catat transaksi keuangan Anda secara rutin',
                onClose: () => this.setCurrentDateInLocalStorage(this.currentDate.getDate().toString())
            })
        }
    }
}