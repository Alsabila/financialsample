import {
    EmptyStep
} from "./step/empty-step";
import {
    AddIncomeStep,
    FormIncomeStep,
    FinishIncomeStep,
    StartIncomeStep
} from "./step/income-step";

export class FeatureIncome implements Feature {
    private empty: EmptyStep;
    private startIncome: StartIncomeStep;
    private addIncome: AddIncomeStep;
    private formIncome: FormIncomeStep;
    private finishIncome: FinishIncomeStep;

    private currentStep: Step;
    private currentStatus: string;

    constructor() {
        this.empty = new EmptyStep();
        this.startIncome = new StartIncomeStep();
        this.addIncome = new AddIncomeStep();
        this.formIncome = new FormIncomeStep();
        this.finishIncome = new FinishIncomeStep();

        this.setStep(this.startIncome);
        this.setStatus("Not Finished");
    }

    getStatus() {
        return this.currentStatus;
    }

    setStatus(status: string) {
        this.currentStatus = status;
    }

    getStep() {
        if (this.currentStatus == "Not Finished") {
            if (window.location.pathname == '/homepage' && this.currentStep instanceof StartIncomeStep) {
                // do nothing
            } else if (window.location.pathname == '/catatan-pemasukan' && this.currentStep instanceof StartIncomeStep) {
                this.setStep(this.addIncome);
            } else if (window.location.pathname == '/catatan-pemasukan' && this.currentStep instanceof FormIncomeStep) {
                this.setStep(this.finishIncome);
                this.setStatus("Finished");
            } else if (window.location.pathname == '/halaman-tambah-pemasukan') {
                this.setStep(this.formIncome);
            } else {
                this.setStep(this.empty);
            }
        } else {
            this.setStep(this.empty);
        }
        return this.currentStep;
    }

    setStep(step: Step) {
        this.currentStep = step;
    }

    getFeatureName() {
        return "catatan-pemasukan";
    }

}