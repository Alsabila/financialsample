import * as introJs from 'intro.js/intro.js';

export class AutomaticReportStep implements Step {
    getExplanation() {
        return {
            doneLabel: "Ok, saya mengerti!",
            steps: [{
                intro: "Anda akan diarahkan untuk melihat Laporan Keuangan"
            }, {
                element: '#button-fitur',
                intro: "Klik menu Fitur pada menu bar lalu pilih fitur Laporan Keuangan"
            }]
        }
    }
}

export class SeeAutomaticReportStep implements Step {
    getExplanation() {
        return {
            doneLabel: "Ok, saya mengerti!",
            steps: [{
                intro: "Selamat datang di halaman Laporan Keuangan"
            }, {
                element: '.table-parent',
                intro: "Laporan ini dibuat secara otomatis dalam format PSAK-45"
            }, {
                element: '.searchTerm',
                intro: "Anda bisa meng-input kode akun untuk melihat laporan keuangan akun tersebut"
            }, {
                element: '#button-fitur',
                intro: "Daftar kode akun bisa dilihat di fitur Daftar Kode Akun"
            }]
        }
    }
}