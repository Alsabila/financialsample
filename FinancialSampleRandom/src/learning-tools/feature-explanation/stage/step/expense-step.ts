export class StartExpenseStep implements Step {
    getExplanation() {
        return {
            doneLabel: "Ok, saya klik Fitur Catatan Pengeluaran!",
            steps: [{
                intro: "Anda akan diarahkan untuk membuat Catatan Pengeluaran baru"
            }, {
                element: '#button-fitur',
                intro: "Klik menu Fitur lalu pilih fitur Catatan Pengeluaran",
            }]
        }
    }
}

export class AddExpenseStep implements Step {
    getExplanation() {
        return {
            doneLabel: "Ayo buat pemasukan baru!",
            steps: [{
                element: '#v-menu-tambah-pengeluaran',
                intro: "Tekan tombol 'Tambah Pengeluaran' untuk menambah pengeluaran baru",
                disableInteraction: true
            }]
        }
    }
}

export class FormExpenseStep implements Step {
    getExplanation() {
        return {
            doneLabel: "Ok, saya buat pengeluaran baru!",
            steps: [{
                intro: "Isi formulir ini dengan informasi mengenai pengeluaran baru Anda",
            }, {
                element: '#input-tanggal',
                intro: "Masukkan tanggal pengeluaran baru Anda. Contoh: Tanggal hari ini"
            }, {
                element: '#input-deskripsi',
                intro: "Masukkan deskripsi mengenai pengeluaran baru Anda. Contoh: My New Expense"
            }, {
                element: '#input-jumlah',
                intro: "Masukkan jumlah pengeluaran baru Anda dalam nilai Rupiah. Contoh: 0"
            }, {
                element: '#input-nama-program-terkait',
                intro: "Masukkan nama program terkait pengeluaran baru Anda. Contoh: My New Program"
            }, {
                element: '#input-kode-akun',
                intro: "Masukkan kode akun pengeluaran baru Anda"
            }, {
                element: '#onsubmit-event-kirim',
                intro: "Setelah formulir dilengkapi, tekan tombol ini untuk mengirim formulir",
                disableInteraction: true
            }]
        }
    }
}

export class FinishExpenseStep implements Step {
    getExplanation() {
        return {
            doneLabel: "Oke, saya berhasil!",
            steps: [{
                intro: "Selamat! Anda berhasil membuat catatan pengeluaran baru!"
            }]
        }
    }
}