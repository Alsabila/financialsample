export class StartIncomeStep implements Step {
    getExplanation() {
        return {
            doneLabel: "Ok, saya klik Fitur Catatan Pemasukan!",
            steps: [{
                intro: "Anda akan diarahkan untuk membuat Catatan Pemasukan baru"
            }, {
                element: '#button-fitur',
                intro: "Klik menu Fitur lalu pilih fitur Catatan Pemasukan",
            }]
        }
    }
}

export class AddIncomeStep implements Step {
    getExplanation() {
        return {
            doneLabel: "Ok, saya klik Tambah Pemasukan!",
            steps: [{
                intro: "Selamat datang di halaman Tambah Pemasukan",
            }, {
                element: '#v-menu-tambah-pemasukan',
                intro: "Tekan tombol 'Tambah Pemasukan' untuk menambah pemasukan baru",
                disableInteraction: true
            }]
        }
    }
}

export class FormIncomeStep implements Step {
    getExplanation() {
        return {
            doneLabel: "Ok, saya buat pemasukan baru!",
            steps: [{
                intro: "Isi formulir ini dengan informasi mengenai pemasukan baru Anda",
            }, {
                element: '#input-tanggal',
                intro: "Masukkan tanggal pemasukan baru Anda. Contoh: tanggal hari ini"
            }, {
                element: '#input-deskripsi',
                intro: "Masukkan deskripsi mengenai pemasukan baru Anda. Contoh: My New Income"
            }, {
                element: '#input-jumlah',
                intro: "Masukkan jumlah pemasukan baru Anda dalam nilai Rupiah. Contoh: 0"
            }, {
                element: '#input-nama-program-terkait',
                intro: "Masukkan nama program terkait pemasukan baru Anda. Contoh: My New Program"
            }, {
                element: '#input-kode-akun',
                intro: "Masukkan kode akun pemasukan baru Anda"
            }, {
                element: '#onsubmit-event-kirim',
                intro: "Setelah formulir dilengkapi, tekan tombol ini untuk mengirim formulir",
                disableInteraction: true
            }]
        }
    }
}

export class FinishIncomeStep implements Step {
    getExplanation() {
        return {
            doneLabel: "Oke, saya berhasil!",
            steps: [{
                intro: "Selamat! Anda berhasil membuat catatan pemasukan baru!"
            }]
        }
    }
}