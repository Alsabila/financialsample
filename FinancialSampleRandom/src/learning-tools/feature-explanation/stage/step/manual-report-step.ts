import * as introJs from 'intro.js/intro.js';

export class TransactionStep implements Step {
    getExplanation() {
        return {
            doneLabel: "Ok, saya mengerti!",
            steps: [{
                intro: "Anda akan diarahkan untuk melihat Catatan Transaksi"
            }, {
                element: '#button-fitur',
                intro: "Klik menu Fitur pada menu bar lalu pilih fitur Catatan Transaksi"
            }]
        }
    }
}

export class SeeTransactionStep implements Step {
    getExplanation() {
        return {
            doneLabel: "Ok, saya mengerti!",
            steps: [{
                intro: "Selamat datang di halaman Catatan Transaksi"
            }, {
                element: '.table-parent',
                intro: "Berikut adalah catatan transaksi yang disajikan AISCO"
            }, {
                element: '.searchTerm',
                intro: "Anda bisa meng-input nama Program untuk melihat catatan transaksi dari program tersebut"
            }]
        }
    }
}