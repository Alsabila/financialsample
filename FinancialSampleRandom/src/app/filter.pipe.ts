import {
    Pipe,
    PipeTransform
} from '@angular/core';

@Pipe({
    name: 'filter'
})
export class FilterPipe implements PipeTransform {
    transform(items: any[], searchText: string): any[] {

        if (!items) {
            return [];
        }
        if (!searchText) {
            return items;
        }
        searchText = searchText.toLocaleLowerCase();

        return items.filter(it => {
            for (let key in it) {
                let value = it[key];
                if (it[key].toLocaleString().toLocaleLowerCase().includes(searchText)) {
                    return true;
                }
            }
            return false;
        });
    }
}