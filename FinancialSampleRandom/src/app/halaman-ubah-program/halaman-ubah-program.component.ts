import {
    Component,
    OnInit
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';

import * as introJs from 'intro.js/intro.js';
import {
    LearningToolSingleton
} from '@learning-tools/learning-tool';

@Component({
    selector: 'halaman-ubah-program',
    templateUrl: './halaman-ubah-program.component.html',
    styleUrls: ['./halaman-ubah-program.component.css']
})
export class HalamanUbahProgramComponent implements OnInit {

    public objectEditProgram: any;
    public searchText: string
    public namaProgram: string;
    public deskripsi: string;
    public target: string;
    public partner: string;
    public urlGambarProgram: string;
    public id: number;
    instance = LearningToolSingleton.getInstance()
    introFeature = introJs();

    constructor(private route: ActivatedRoute, private router: Router) {
        if (localStorage.getItem("Learning Tool Status") == "Done") {
            this.instance.setShowIntroJS(false);
            this.instance.setShowLearningTools(false);
        } else {
            this.setLearningTool()
        }
    }

    setLearningTool() {
        this.introFeature.setOptions(this.instance.getExplanationTooltip());
        this.introFeature.setOptions(this.instance.getFeatureExplanationIntroJSText());

        if (this.instance.getFeatureExplanationStatus() == "Finished") {
            this.instance.writeExplanationInLocalStorage();
            this.introFeature.oncomplete(() => {
                this.instance.setShowIntroJS(false);
                this.instance.getAssessmentResult();
            })
        }
    }

    ngOnInit() {
        if (this.instance.showIntroJS()) {
            this.introFeature.start();
        }
        this.route.queryParams.subscribe(params => {
            this.objectEditProgram = JSON.parse(params.objectEditProgram);
            this.namaProgram = JSON.parse(params.namaProgram);
            this.deskripsi = JSON.parse(params.deskripsi);
            this.target = JSON.parse(params.target);
            this.partner = JSON.parse(params.partner);
            this.urlGambarProgram = JSON.parse(params.urlGambarProgram);
            this.id = JSON.parse(params.id);
        });
    }



}