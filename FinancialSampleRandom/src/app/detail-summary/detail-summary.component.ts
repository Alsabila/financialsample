import {
    Component,
    OnInit,
    Input
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';
import {
    Summary
} from '../models/summary.model';
import {
    ApiSummaryDeleteAbsService
} from '../services/api-summary-delete-abs.service';

import * as introJs from 'intro.js/intro.js';
import {
    LearningToolSingleton
} from '@learning-tools/learning-tool';

@Component({
    selector: 'detail-summary',
    templateUrl: './detail-summary.component.html',
    styleUrls: ['./detail-summary.component.css']
})
export class DetailSummaryComponent implements OnInit {

    @Input() objectDetailSummary: any;
    @Input() idSummary: number;
    public summaryData: Summary;
    instance = LearningToolSingleton.getInstance()
    introFeature = introJs();

    constructor(private route: ActivatedRoute, private router: Router, public apisummarydeleteabsservice: ApiSummaryDeleteAbsService) {
        if (localStorage.getItem("Learning Tool Status") == "Done") {
            this.instance.setShowIntroJS(false);
            this.instance.setShowLearningTools(false);
        } else {
            this.setLearningTool()
        }
    }

    setLearningTool() {
        this.introFeature.setOptions(this.instance.getExplanationTooltip());
        this.introFeature.setOptions(this.instance.getFeatureExplanationIntroJSText());

        if (this.instance.getFeatureExplanationStatus() == "Finished") {
            this.instance.writeExplanationInLocalStorage();
            this.introFeature.oncomplete(() => {
                this.instance.setShowIntroJS(false);
                this.instance.getAssessmentResult();
            })
        }
    }

    ngOnInit() {
        if (this.instance.showIntroJS()) {
            this.introFeature.start();
        }
        this.attachsummarydata();
    }

    attachsummarydata() {
        this.summaryData = this.objectDetailSummary;
        this.idSummary = this.summaryData.id;
    }
    delete() {
        this.apisummarydeleteabsservice.call({
            id: this.idSummary
        }).then(data => {
            this.router.navigate(['/catatan-transaksi'], {
                queryParams: {
                    jsonAllSummary: JSON.stringify(data['data'])
                }
            });
        }).catch(error => {
            this.router.navigate(['/catatan-transaksi'], {
                queryParams: {
                    jsonAllSummary: JSON.stringify([])
                }
            });
        });
    }

}