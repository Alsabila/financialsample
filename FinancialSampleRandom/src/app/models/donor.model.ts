export class Donor {
    public id: number;
    public name: string;
    public email: string;
    public phone: string;
    public address: string;

    constructor(obj ? : any) {
        this.id = obj && obj.id || null;
        this.name = obj && obj.name || null;
        this.email = obj && obj.email || null;
        this.phone = obj && obj.phone || null;
        this.address = obj && obj.address || null;
    }


}