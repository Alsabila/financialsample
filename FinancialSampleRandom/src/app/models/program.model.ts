export class Program {
    public id: number;
    public name: string;
    public description: string;
    public target: string;
    public partner: string;
    public logoUrl: string;

    constructor(obj ? : any) {
        this.id = obj && obj.id || null;
        this.name = obj && obj.name || null;
        this.description = obj && obj.description || null;
        this.target = obj && obj.target || null;
        this.partner = obj && obj.partner || null;
        this.logoUrl = obj && obj.logoUrl || null;
    }


}