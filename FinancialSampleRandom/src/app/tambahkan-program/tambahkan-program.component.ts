import {
    Component,
    OnInit
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';
import {
    FileUploadService
} from '../services/file-upload.service';
import {
    ApiProgramSaveAbsService
} from '../services/api-program-save-abs.service';

import * as introJs from 'intro.js/intro.js';
import {
    LearningToolSingleton
} from '@learning-tools/learning-tool';

@Component({
    selector: 'tambahkan-program',
    templateUrl: './tambahkan-program.component.html',
    styleUrls: ['./tambahkan-program.component.css']
})
export class TambahkanProgramComponent implements OnInit {

    public namaProgram: string;
    public deskripsi: string;
    public target: string;
    public partner: string;
    public urlGambarProgram: string;
    public fileToUpload: File;
    instance = LearningToolSingleton.getInstance()
    introFeature = introJs();

    constructor(private route: ActivatedRoute, private router: Router, public fileUploadService: FileUploadService, public apiprogramsaveabsservice: ApiProgramSaveAbsService) {
        if (localStorage.getItem("Learning Tool Status") == "Done") {
            this.instance.setShowIntroJS(false);
            this.instance.setShowLearningTools(false);
        } else {
            this.setLearningTool()
        }
    }

    setLearningTool() {
        this.introFeature.setOptions(this.instance.getExplanationTooltip());
        this.introFeature.setOptions(this.instance.getFeatureExplanationIntroJSText());

        if (this.instance.getFeatureExplanationStatus() == "Finished") {
            this.instance.writeExplanationInLocalStorage();
            this.introFeature.oncomplete(() => {
                this.instance.setShowIntroJS(false);
                this.instance.getAssessmentResult();
            })
        }
    }

    ngOnInit() {
        if (this.instance.showIntroJS()) {
            this.introFeature.start();
        }

    }

    handleFileInput(files: FileList) {
        this.fileToUpload = files.item(0);
        console.log('In..')
        console.log(this.fileToUpload)
        this.uploadFileToActivity();
    }

    resetCss() {
        var current = document.getElementsByClassName("done");
        if (current.length > 0) {
            current[0].className = current[0].className.replace(" done", "");
        }
    }

    uploadFileToActivity() {
        this.fileUploadService.postFile(this.fileToUpload).subscribe(data => {
            this.urlGambarProgram = data;
            var currentActive = document.getElementById("input-url-gambar-program");
            currentActive.className += " done";
        }, error => {
            console.log(error);
        });
    }
    kirim() {
        this.apiprogramsaveabsservice.call({
            name: this.namaProgram,
            description: this.deskripsi,
            target: this.target,
            partner: this.partner,
            logoUrl: this.urlGambarProgram
        }).then(data => {
            this.instance.updateAssessmentData("Program", data['data'].length);
            if (localStorage.getItem("Explanation Status") == "Finished") {
                this.instance.getAssessmentResult();
            }
            this.router.navigate(['/daftar-program'], {
                queryParams: {
                    jsonAllProgram: JSON.stringify(data['data'])
                }
            });
        }).catch(error => {
            this.router.navigate(['/daftar-program'], {
                queryParams: {
                    jsonAllProgram: JSON.stringify([])
                }
            });
        });
    }

}