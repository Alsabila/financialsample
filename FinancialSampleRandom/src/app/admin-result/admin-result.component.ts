import {
    Component,
    OnInit
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';

@Component({
    selector: 'app-admin-result',
    templateUrl: './admin-result.component.html',
    styleUrls: ['./admin-result.component.css']
})
export class AdminResultComponent implements OnInit {

    static counter: number;

    constructor(private route: ActivatedRoute, private router: Router) {
        AdminResultComponent.counter = 5;
    }

    ngOnInit() {
        this.countDown();
    }

    countDown() {
        setInterval(function() {
            // Get today's date and time
            AdminResultComponent.counter--;
            document.getElementById("timer").innerHTML = "Websitemu sudah SIAP digunakan dalam " + AdminResultComponent.counter + " detik";

            // If the count down is finished, write some text 
            if (AdminResultComponent.counter <= 0) {
                window.location.href = "/homepage";
            }
        }, 1000);
    }

    go() {
        this.router.navigate(['/homepage'])
            .then(() => {
                window.location.reload();
            });
    }

}