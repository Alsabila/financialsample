import {
    Component,
    OnInit
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';
import {
    StaticPageService
} from '../services/static-page.service';
import {
    FileUploadService
} from '../services/file-upload.service';

@Component({
    selector: 'app-admin-edit',
    templateUrl: './admin-edit.component.html',
    styleUrls: ['./admin-edit.component.css']
})
export class AdminEditComponent implements OnInit {

    public detail: string;
    public address: string;
    public map: string;
    public email: string;
    public phone: string;
    public logoUrl: string;
    public banner: string;
    public aboutUs: string;
    public slogan: string;
    public facebook: string;
    public instagram: string;
    public styles: string;
    public fileToUpload: File;

    constructor(private route: ActivatedRoute, private router: Router,
        public staticservice: StaticPageService, public fileUploadService: FileUploadService) {}

    ngOnInit() {
        this.staticservice.getstatic().then(data => {
            this.detail = data["page"]["description"];
            this.aboutUs = data["page"]["about_us"];
            this.slogan = data["page"]["slogan"];
            this.address = data["contact"]["address"];
            this.map = data["contact"]["url"]["google_map_url"];
            this.email = data["contact"]["email"];
            this.phone = data["contact"]["phone"];
            this.facebook = data["contact"]["url"]["fb_url"];
            this.instagram = data["contact"]["url"]["instagram_url"];
            this.logoUrl = data["essential_assets"]["logo_url"];
            this.banner = data["essential_assets"]["banner_url"];
            this.styles = data["styles"];
            var idCss = this.styles + "-theme";
            var currentActive = document.getElementById(idCss);
            currentActive.className += " active";
        });
    }

    select(color) {
        console.log(color);
        this.styles = color;
        var idCss = color + "-theme";
        var header = document.getElementById(idCss);
        var current = document.getElementsByClassName("active");
        current[0].className = current[0].className.replace(" active", "");
        header.className += " active";
    }

    handleFileInput(files: FileList, name: string) {
        this.fileToUpload = files.item(0);
        console.log('In..');
        console.log(this.fileToUpload);
        console.log();
        this.uploadFileToActivity(name);
    }

    resetCss(name) {
        if (name === "banner") {
            var current = document.getElementById("input-banner");
            current.className = current.className.substring(0, current.className.length - 4);
        } else {
            var current = document.getElementById("input-logo");
            current.className = current.className.substring(0, current.className.length - 4);
        }
    }

    uploadFileToActivity(name: string) {
        this.fileUploadService.postFile(this.fileToUpload).subscribe(data => {
            console.log(data);
            if (name === 'banner') {
                this.banner = data;
                var currentActive = document.getElementById("input-banner");
                currentActive.className += " done";
            } else {
                this.logoUrl = data;
                var currentActive = document.getElementById("input-logo");
                currentActive.className += " done";
            }
        }, error => {
            console.log(error);
        });
    }

    submit() {
        this.staticservice.poststatic({
                "about_page": {
                    "description": [
                        this.detail
                    ],
                    "about_us": this.aboutUs,
                    "slogan": this.slogan
                },
                "contact": {
                    "address": this.address,
                    "email": this.email,
                    "google_map_url": this.map,
                    "phone": this.phone,
                    "fb_url": this.facebook,
                    "instagram_url": this.instagram
                },
                "essential_assets": {
                    "bannerUrl": this.banner,
                    "logoUrl": this.logoUrl
                },
                "styles": this.styles
            }

        ).then(data => {
            console.log(data);
            if (data['status'] == 200) {
                this.router.navigate(['/admin/result']);
            }
        });
    }

}