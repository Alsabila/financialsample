import {
    Injectable
} from '@angular/core';
import {
    environment
} from '../../environments/environment';
import {
    HttpClient,
    HttpHeaders,
    HttpParams
} from '@angular/common/http';

import {
    Observable
} from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class FileUploadService {

    constructor(private httpClient: HttpClient) {

    }

    httpOptions = {

        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    };


    postFile(fileToUpload: File): Observable < any > {
        const endpoint = environment.rootStaticApi + 'apiimage/upload';
        const formData: FormData = new FormData();
        formData.append('fileKey', fileToUpload, fileToUpload.name);
        return this.httpClient
            .post(endpoint, formData);
    }
}