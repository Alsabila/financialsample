import {
    Component,
    OnInit,
    Input
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';
import {
    Program
} from '../models/program.model';
import {
    ApiProgramDeleteAbsService
} from '../services/api-program-delete-abs.service';

import * as introJs from 'intro.js/intro.js';
import {
    LearningToolSingleton
} from '@learning-tools/learning-tool';

@Component({
    selector: 'detail-program',
    templateUrl: './detail-program.component.html',
    styleUrls: ['./detail-program.component.css']
})
export class DetailProgramComponent implements OnInit {

    @Input() objectDetailProgram: any;
    @Input() id: number;
    public programData: Program;
    instance = LearningToolSingleton.getInstance()
    introFeature = introJs();

    constructor(private route: ActivatedRoute, private router: Router, public apiprogramdeleteabsservice: ApiProgramDeleteAbsService) {
        if (localStorage.getItem("Learning Tool Status") == "Done") {
            this.instance.setShowIntroJS(false);
            this.instance.setShowLearningTools(false);
        } else {
            this.setLearningTool()
        }
    }

    setLearningTool() {
        this.introFeature.setOptions(this.instance.getExplanationTooltip());
        this.introFeature.setOptions(this.instance.getFeatureExplanationIntroJSText());

        if (this.instance.getFeatureExplanationStatus() == "Finished") {
            this.instance.writeExplanationInLocalStorage();
            this.introFeature.oncomplete(() => {
                this.instance.setShowIntroJS(false);
                this.instance.getAssessmentResult();
            })
        }
    }

    ngOnInit() {
        if (this.instance.showIntroJS()) {
            this.introFeature.start();
        }
        this.attachprogramdata();
    }

    attachprogramdata() {
        this.programData = this.objectDetailProgram;
        this.id = this.programData.id;
    }
    hapus() {
        this.apiprogramdeleteabsservice.call({
            id: this.id
        }).then(data => {
            this.router.navigate(['/daftar-program'], {
                queryParams: {
                    jsonAllProgram: JSON.stringify(data['data'])
                }
            });
        }).catch(error => {
            this.router.navigate(['/daftar-program'], {
                queryParams: {
                    jsonAllProgram: JSON.stringify([])
                }
            });
        });
    }
    ubah() {
        this.router.navigate(['/halaman-ubah-program'], {
            queryParams: {
                objectEditProgram: JSON.stringify(this.objectDetailProgram)
            }
        });
    }

}