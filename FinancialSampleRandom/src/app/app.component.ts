import {
    Component
} from '@angular/core';

import {routes } from './app-routing.module';
import {
    LearningToolSingleton
} from '@learning-tools/learning-tool';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {


    instance = LearningToolSingleton.getInstance()

    constructor() {
        this.instance.generateLearningTool(routes);
    }
}