export class LevelAssessment {
    private currentAssessment: AssessmentInterface;

    constructor(assessment: AssessmentInterface) {
        this.currentAssessment = assessment;
    }

    public getHtmlTemplate() {
        return `<table style="border-collapse: collapse; width: 100%; border: transparent; bgcolor: transparent;">
        <tbody>
        <tr style="height: 20px; ">
        <th style="width: 100%; text-align: center; height: 30px; font-weight: bold; font-size: 12px; background-color: #f2f2f2" colspan="2">PENCAPAIAN FITUR SAAT INI</th>
        </tr>` +
            this.currentAssessment.getHtmlTemplateRequirements() +
            `</tbody>
        </table>
        <table style="border-collapse: collapse; width: 100%; border: transparent;">
        <tbody>
        <tr style="height: 20px; background-color: transparent">
        <th style="text-align: center; width: 100%; height: 30px; font-weight: bold; font-size: 12px; background-color: #f2f2f2" colspan="2">FITUR BARU YANG DAPAT DIGUNAKAN</th>
        </tr>
        <tr style="height: 70px; background-color: transparent">` +
            this.currentAssessment.getHtmlTemplateNextFeature() +
            `</tr>
        </tbody>
        </table>`
    }

    public getAssessment() {
        return this.currentAssessment;
    }

    public setAssessment(assessment: AssessmentInterface) {
        this.currentAssessment = assessment;
    }
}