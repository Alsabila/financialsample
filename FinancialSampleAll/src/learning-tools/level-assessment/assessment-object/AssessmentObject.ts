export class Requirement {
    private description: string;
    private total: number;
    private currentTotal: number;

    constructor(desc: string, total: number, curr: number) {
        this.description = desc;
        this.total = total;
        this.currentTotal = curr;
    }

    public getDescription() {
        return this.description;
    }
    public getTotal() {
        return this.total;
    }
    public getCurrentTotal() {
        return this.currentTotal;
    }

    public setCurrentTotal(num: number) {
        this.currentTotal = num;
    }

    public getHtmlTemplate() {
        return `<tr style="height: 20px; background-color: transparent">
        <td style="width: 50%; text-align: center; height: 50px; font-size: 12px; background-color: transparent">` + this.description + `</td>
        <td style="width: 50%; text-align: center; height: 50px; font-size: 12px; background-color: transparent">` + this.currentTotal + ` / ` + this.total + `</td>
        </tr>`
    }
}

export class NextFeature {
    private name: string;
    private description: string;
    private image: string;

    constructor(name: string, desc: string, img: string) {
        this.name = name;
        this.description = desc;
        this.image = img;
    }

    public getName() {
        return this.name
    };
    public getDescription() {
        return this.description
    };
    public getImage() {
        return this.image
    };

    public getHtmlTemplate() {
        return `<td style="text-align: center; width: 50%; height: 30px; background-color: transparent"><img src="` + this.image + `" alt="" width="50" height="50" /></td>
        <td style="width: 50%; text-align: justify; height: 30px; background-color: transparent">
        <p style="font-weight: bold; font-size: 12px;">` + this.name + `:&nbsp;</p>
        <p style="text-align: left; font-size: 12px;">` + this.description + `</p>
        </td>`
    }
}