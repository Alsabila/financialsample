interface AssessmentInterface {
    updateData(name: string, count: number);
    getRequirements();
    getNextFeature();
    getHtmlTemplateRequirements();
    getHtmlTemplateNextFeature();
    getStatus();
    getAssessmentName();
    modifyNextFeature(nextFeatureName: string);
}