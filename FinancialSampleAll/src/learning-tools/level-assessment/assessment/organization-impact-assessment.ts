import {
    Assessment
} from "./Assessment";
import {
    Requirement,
    NextFeature
} from "../assessment-object/AssessmentObject";

export class AssessmentThree extends Assessment {
    private req1 = new Requirement("Jumlah program yang sedang/telah berlangsung mencapai 5 program", 3, 0);
    private req2 = new Requirement("Jumlah pengeluaran mencapai 100 transaksi", 3, 0);

    private nextFeatureSummary = new NextFeature("Catatan Transaksi", "Menyajikan catatan transaksi keuangan organisasi Anda",
        require('../../../assets/images/manual.png'));
    private nextFeatureReport = new NextFeature("Laporan Keuangan", "Membuat laporan otomatis sesuai format PSAK-45",
        require('../../../assets/images/automatic_45.png'));
    private nextFeatureEmpty = new NextFeature("", "", "");

    constructor() {
        super();
        this.updateData("All", 0);
        this.setRequirements([this.req1, this.req2]);
        this.setNextFetures(this.nextFeatureSummary);
    }

    getProgramCount() {
        fetch('http://34.101.241.250/api/program/list.abs').then(res => res.json())
            .then(data => this.req1.setCurrentTotal(data['data'].length))
            .catch(error => console.log("Error"));
    }

    getExpenseCount() {
        fetch('http://34.101.241.250/api/expense/list.abs').then(res => res.json())
            .then(data => this.req2.setCurrentTotal(data['data'].length))
            .catch(error => console.log("Error"));

    }

    modifyNextFeature(nextFeatureName: string) {
        if (nextFeatureName == "laporan-keuangan") this.setNextFetures(this.nextFeatureReport);
        else if (nextFeatureName == "empty") this.setNextFetures(this.nextFeatureEmpty);
        else console.log("Already constructed");
    }

    updateData(name: string, count: number) {
        if (name == "All") {
            this.getProgramCount();
            this.getExpenseCount();
        } else if (name == "Program") {
            this.req1.setCurrentTotal(count);
        } else if (name == "Expense") {
            this.req2.setCurrentTotal(count);
        }
    }

    getStatus() {
        if (this.req1.getCurrentTotal() >= this.req1.getTotal() &&
            this.req2.getCurrentTotal() >= this.req2.getTotal()) {
            return "Achieved";
        } else {
            return "Not Achieved";
        }
    }

    getAssessmentName() {
        return "catatan-pengeluaran";
    }
}