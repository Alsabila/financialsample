import Swal from 'sweetalert2';
import {
    Router
} from '@angular/router';

export class FeatureAdvisor {
    private feature = "";
    private nextFeatHtml = "";

    async getAdvisorForNextFeature(feature: string) {
        this.feature = feature;
        if (this.feature == "catatan-pemasukan") {
            this.nextFeatHtml = '<p style="color:#545454; font-family: Verdana,sans-serif;">Catatan Pemasukan</p>';
        } else if (this.feature == "catatan-pengeluaran") {
            this.nextFeatHtml = '<p style="color:#545454; font-family: Verdana,sans-serif;">Catatan Pengeluaran</p>';
        } else if (this.feature == "catatan-transaksi") {
            this.nextFeatHtml = '<p style="color:#545454; font-family: Verdana,sans-serif;">Catatan Transaksi</p>';
        } else if (this.feature == "laporan-keuangan") {
            this.nextFeatHtml = '<p style="color:#545454; font-family: Verdana,sans-serif;">Laporan Keuangan</p>';
        }

        const {
            value: accept
        } = await Swal.fire({
            title: 'Saatnya Mencoba yang Baru!',
            text: 'Selamat! Organisasi Anda mengalami perkembangan! Saatnya mencoba fitur baru yang dapat digunakan',
            input: 'checkbox',
            imageUrl: require('../../assets/images/feature-advisor.png'),
            imageWidth: 50,
            imageHeight: 50,
            imageAlt: 'Custom image',
            inputPlaceholder: this.nextFeatHtml,
            confirmButtonText: 'Tambahkan <i class="fa fa-plus"></i>',
            inputValidator: (result) => {
                return !result && 'Mohon pilih fitur yang ingin ditambahkan'
            }
        })

        if (accept) {
            Swal.fire({
                title: 'Fitur baru berhasil ditambahkan!',
                onClose: () => {
                    window.location.replace('homepage');
                }
            });
        }
    }
}