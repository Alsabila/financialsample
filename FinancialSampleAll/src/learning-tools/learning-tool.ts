import {
    FeatureExplanation
} from "./feature-explanation/feature-explanation";
import {
    LevelAssessment
} from "./level-assessment/level-assessment";
import {
    FinancialReminder
} from "./financial-reminder/financial-reminder";
import {
    FeatureProgram
} from "./feature-explanation/stage/organization-need-stage";
import {
    AssessmentOne
} from "./level-assessment/assessment/organization-need-assessment";
import {
    FeatureIncome
} from "./feature-explanation/stage/service-need-stage";
import {
    AssessmentTwo
} from "./level-assessment/assessment/service-need-assessment";
import {
    FeatureExpense
} from "./feature-explanation/stage/organization-impact-stage";
import {
    AssessmentThree
} from "./level-assessment/assessment/organization-impact-assessment";
import {
    FeatureManRep,
    FeatureAutRep
} from "./feature-explanation/stage/donor-impact-stage";
import {
    Router,
    Routes
} from "@angular/router";
import {
    FeatureAdvisor
} from "./feature-advisor/feature-advisor";
import Swal from "sweetalert2";
import {
    EmptyAssessment
} from "./level-assessment/assessment/empty-assessment";

export class LearningToolSingleton {
    private static instance = new LearningToolSingleton();

    private listFeatureVariables = {};
    private showIntro = true;
    private showLearningTool = true;
    private listRoutes: Routes;

    private featureProgram = new FeatureProgram();
    private featureIncome = new FeatureIncome();
    private featureExpense = new FeatureExpense();
    private featureAuto = new FeatureAutRep();
    private FeatureMan = new FeatureManRep();

    private assessmentOne = new AssessmentOne();
    private assessmentTwo = new AssessmentTwo();
    private assessmentThree = new AssessmentThree();
    private emptyAssessment = new EmptyAssessment();

    private featureExplanation = new FeatureExplanation(this.featureProgram);
    private levelAssessment = new LevelAssessment(this.assessmentOne);
    private featureAdvisor = new FeatureAdvisor();
    private financialReminder = new FinancialReminder();

    private constructor() {}

    public static getInstance() {
        return this.instance;
    }

    public getFeatureExplanation() {
        return this.featureExplanation;
    }

    public setFeatureExplanation(feature: Feature) {
        this.featureExplanation = new FeatureExplanation(feature);
    }

    public getLevelAssessment() {
        return this.levelAssessment;
    }

    public setLevelAssessment(assessment: AssessmentInterface) {
        this.levelAssessment = new LevelAssessment(assessment);
    }

    public showIntroJS() {
        return this.showIntro;
    }

    public setShowIntroJS(show: boolean) {
        this.showIntro = show;
    }

    public showLearningTools() {
        return this.showLearningTool;
    }

    public setShowLearningTools(show: boolean) {
        this.showLearningTool = show;
    }

    public getExplanationTooltip() {
        return {
            exitOnOverlayClick: false,
            exitOnEsc: true,
            hidePrev: true,
            hideNext: true,
            showBullets: false,
            showStepNumbers: false,
            overlayOpacity: 0.1,
            nextLabel: "Ok, mengerti!",
            keyboardNavigation: false,
            prevLabel: "Kembali lagi"
        }
    }

    public getFeatureExplanationStatus() {
        return this.getFeatureExplanation().getFeature().getStatus();
    }

    public getFeatureExplanationIntroJSText() {
        return this.getFeatureExplanation().getFeature().getStep().getExplanation();
    }

    public getFinancialReminderAlert() {
        if (localStorage.getItem("Current Feature") != "program") {
            if (!this.showIntro) {
                this.financialReminder.showReminderAtFour();
            }
        }
    }

    public generateLearningTool(routes: Routes) {
        this.listRoutes = routes;

        this.generateAllFeatures();
        try {
            this.retriveCurrentFeatureAndAssessmentFromLocalStorage();
        } catch (error) {
            this.generateFeatureExplanation("program", "Not Finished");
            this.generateLevelAssessment("program");
        }
    }

    public generateAllFeatures() {
        this.modifyNextFeatureInAssessments();
        this.listFeatureVariables["program"] = {
            feature: this.featureProgram,
            assessment: this.assessmentOne
        };
        this.listFeatureVariables["catatan-pemasukan"] = {
            feature: this.featureIncome,
            assessment: this.assessmentTwo
        };
        this.listFeatureVariables["empty"] = {
            feature: null,
            assessment: this.emptyAssessment
        }
        if (this.isRouteExist("catatan-pengeluaran")) {
            this.listFeatureVariables["catatan-pengeluaran"] = {
                feature: this.featureExpense,
                assessment: this.assessmentThree
            };
        }
        if (this.isRouteExist("catatan-transaksi")) {
            this.listFeatureVariables["catatan-transaksi"] = {
                feature: this.FeatureMan,
                assessment: this.emptyAssessment
            };
        }
        if (this.isRouteExist("laporan-keuangan")) {
            this.listFeatureVariables["laporan-keuangan"] = {
                feature: this.featureAuto,
                assessment: this.emptyAssessment
            };
        }
    }

    public isRouteExist(route: string) {
        let listRoute = this.listRoutes.find(r => r.path == '');
        return listRoute.children.findIndex(r => r.path == route) != -1;
    }

    public generateFeatureExplanation(featureName: string, status: string) {
        try {
            let curentFeature = this.listFeatureVariables[featureName]['feature'];
            this.featureExplanation = new FeatureExplanation(curentFeature);
            this.featureExplanation.getFeature().setStatus(status);
            this.writeExplanationInLocalStorage();
        } catch (error) {
            console.log(error);
        }
    }

    public generateLevelAssessment(featureName: string) {
        try {
            let currentAssessment = this.listFeatureVariables[featureName]['assessment'];
            this.levelAssessment = new LevelAssessment(currentAssessment);
            this.writeAssessmentInLocalStorage();
        } catch (error) {
            console.log(error);
        }
    }

    public modifyNextFeatureInAssessments() {
        if (this.isRouteExist("catatan-pengeluaran")) {
            if (this.isRouteExist("catatan-transaksi")) { // do nothing
            } else {
                if (this.isRouteExist("laporan-keuangan")) this.assessmentThree.modifyNextFeature("laporan-keuangan");
                else this.assessmentThree.modifyNextFeature("empty");
            }
        } else {
            if (this.isRouteExist("catatan-transaksi")) this.assessmentTwo.modifyNextFeature("catatan-transaksi");
            else {
                if (this.isRouteExist("laporan-keuangan")) this.assessmentTwo.modifyNextFeature("laporan-keuangan");
                else this.assessmentTwo.modifyNextFeature("empty");
            }
        }
    }

    public writeExplanationInLocalStorage() {
        localStorage.setItem("Current Feature", this.featureExplanation.getFeature().getFeatureName());
        localStorage.setItem("Explanation Status", this.featureExplanation.getFeature().getStatus());
    }

    public writeAssessmentInLocalStorage() {
        localStorage.setItem("Current Assessment", this.levelAssessment.getAssessment().getAssessmentName());
        localStorage.setItem("Assessment Status", this.levelAssessment.getAssessment().getStatus());
    }

    public updateAssessmentData(name: string, count: number) {
        this.levelAssessment.getAssessment().updateData(name, count);
        this.writeAssessmentInLocalStorage();
    }

    public retrieveFeatureExplanationFromLocalStorage() {
        let featureName = localStorage.getItem("Current Feature");
        let currentStatus = localStorage.getItem("Explanation Status");
        this.generateFeatureExplanation(featureName, currentStatus);
        if (currentStatus == "Not Finished") {
            this.setShowIntroJS(true);
        } else {
            this.setShowIntroJS(false);
        }
    }

    public retrieveLevelAssessmentFromLocalStorage() {
        let featureName = localStorage.getItem("Current Assessment");
        this.generateLevelAssessment(featureName);
    }

    public retriveCurrentFeatureAndAssessmentFromLocalStorage() {
        if (localStorage.getItem("Current Feature") != null) this.retrieveFeatureExplanationFromLocalStorage();
        else this.writeExplanationInLocalStorage();

        if (localStorage.getItem("Current Assessment") != null) this.retrieveLevelAssessmentFromLocalStorage();
        else this.writeAssessmentInLocalStorage();
    }

    public getAssessmentResult() {
        if (this.showLearningTool) {
            if (this.levelAssessment.getAssessment().getStatus() == "Not Achieved") {
                Swal.fire({
                    title: "Tingkat Pencapaian",
                    html: this.levelAssessment.getHtmlTemplate(),
                    allowOutsideClick: false,
                    allowEnterKey: false,
                    allowEscapeKey: false,
                });
            } else {
                let currentFeature = localStorage.getItem("Current Feature");
                if (currentFeature == "program" || currentFeature == "catatan-pemasukan" || currentFeature == "catatan-pengeluaran") {
                    Swal.fire({
                        title: "Tingkat Pencapaian",
                        html: this.levelAssessment.getHtmlTemplate(),
                        allowOutsideClick: false,
                        allowEnterKey: false,
                        allowEscapeKey: false,
                        onClose: () => this.goToNextFeature(currentFeature),
                    })
                } else if (currentFeature == "catatan-transaksi") {
                    this.goToNextFeature(currentFeature);
                } else if (currentFeature == "laporan-keuangan") {
                    this.endLearningTool();
                }
            }
        }
    }

    public goToNextFeature(featureName: string) {
        this.setShowIntroJS(true);
        if (featureName == "program") {
            this.generateNextFeatureLearningTools("catatan-pemasukan", "Not Finished")
        } else if (featureName == "catatan-pemasukan") {
            if (this.isRouteExist("catatan-pengeluaran")) this.generateNextFeatureLearningTools("catatan-pengeluaran", "Not Finished");
            else {
                if (this.isRouteExist("catatan-transaksi")) this.generateNextFeatureLearningTools("catatan-transaksi", "Not Finished");
                else if (this.isRouteExist("laporan-keuangan")) this.generateNextFeatureLearningTools("laporan-keuangan", "Not Finished");
                else this.endLearningTool();
            }
        } else if (featureName == "catatan-pengeluaran") {
            if (this.isRouteExist("catatan-transaksi")) this.generateNextFeatureLearningTools("catatan-transaksi", "Not Finished");
            else if (this.isRouteExist("laporan-keuangan")) this.generateNextFeatureLearningTools("laporan-keuangan", "Not Finished");
            else this.endLearningTool();
        } else if (featureName == "catatan-transaksi") {
            if (this.isRouteExist("laporan-keuangan")) this.generateNextFeatureLearningTools("laporan-keuangan", "Not Finished");
            else this.endLearningTool();
        }
    }

    public generateNextFeatureLearningTools(featureName: string, status: string) {
        this.generateFeatureExplanation(featureName, status);
        this.generateLevelAssessment(featureName);
        this.featureAdvisor.getAdvisorForNextFeature(featureName);
    }

    public endLearningTool() {
        Swal.fire({
            title: "Selamat!",
            text: "Anda telah menyelesaiakan pembelajaran sistem informasi AISCO",
            allowOutsideClick: false,
            allowEnterKey: false,
            allowEscapeKey: false,
        });
        localStorage.setItem("Learning Tool Status", "Done");
        this.setShowLearningTools(false);
    }
}