import {
    EmptyStep
} from "./step/empty-step";
import {
    TransactionStep,
    SeeTransactionStep
} from "./step/manual-report-step";
import {
    AutomaticReportStep,
    SeeAutomaticReportStep
} from "./step/automatic-report-step";



export class FeatureManRep implements Feature {
    private empty: EmptyStep;
    private transaction: TransactionStep;
    private seeTransaction: SeeTransactionStep;

    private currentStep: Step;
    private currentStatus: string;

    constructor() {
        this.empty = new EmptyStep();

        this.transaction = new TransactionStep();
        this.seeTransaction = new SeeTransactionStep();

        this.setStep(this.transaction);
        this.setStatus("Not Finished");
    }

    getStatus() {
        return this.currentStatus;
    }

    setStatus(status: string) {
        this.currentStatus = status;
    }

    getStep() {
        if (this.currentStatus == "Not Finished") {
            if (window.location.pathname == '/homepage' && this.currentStep instanceof TransactionStep) {
                // do nothing
            } else if (window.location.pathname == '/catatan-transaksi' && this.currentStep instanceof TransactionStep) {
                this.setStep(this.seeTransaction);
                this.setStatus("Finished");
            } else {
                this.setStep(this.empty);
            }
        } else {
            this.setStep(this.empty);
        }
        return this.currentStep;
    }

    setStep(step: Step) {
        this.currentStep = step;
    }

    getFeatureName() {
        return "catatan-transaksi";
    }

}

export class FeatureAutRep implements Feature {
    private empty: EmptyStep;
    private autoReport: AutomaticReportStep;
    private seeAutoReport: SeeAutomaticReportStep;

    private currentStep: Step;
    private currentStatus: string;

    constructor() {
        this.empty = new EmptyStep();

        this.autoReport = new AutomaticReportStep();
        this.seeAutoReport = new SeeAutomaticReportStep();

        this.setStep(this.autoReport);
        this.setStatus("Not Finished");
    }

    getStatus() {
        return this.currentStatus;
    }

    setStatus(status: string) {
        this.currentStatus = status;
    }

    getStep() {
        if (this.currentStatus == "Not Finished") {
            if (window.location.pathname == '/homepage' && this.currentStep instanceof AutomaticReportStep) {
                // do nothing
            } else if (window.location.pathname == '/laporan-keuangan' && this.currentStep instanceof AutomaticReportStep) {
                this.setStep(this.seeAutoReport);
                this.setStatus("Finished");
            } else {
                this.setStep(this.empty);
            }
        } else {
            this.setStep(this.empty);
        }
        return this.currentStep;
    }

    setStep(step: Step) {
        this.currentStep = step;
    }

    getFeatureName() {
        return "laporan-keuangan";
    }

}