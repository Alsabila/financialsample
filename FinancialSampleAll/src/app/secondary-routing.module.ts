import {
    NgModule
} from '@angular/core';
import {
    RouterModule,
    Routes
} from '@angular/router';

import {
    AdminMainComponent
} from './admin-main/admin-main.component'
import {
    AdminLoginComponent
} from './admin-login/admin-login.component';
import {
    AdminEditComponent
} from './admin-edit/admin-edit.component';
import {
    AdminResultComponent
} from './admin-result/admin-result.component';
import {
    AuthLoginAdminGuard
} from './guard/auth-login-admin.guard'
import {
    AuthNotLoginAdminGuard
} from './guard/auth-not-login-admin.guard'
import {
    AdminAuthGuard
} from './guard/auth-admin.guard'

const secondaryRoutes: Routes = [{
    path: 'admin',
    component: AdminMainComponent,
    children: [{
        path: '',
        redirectTo: 'edit',
        pathMatch: 'full'
    }, {
        path: 'login',
        component: AdminLoginComponent,
        canActivate: [AuthNotLoginAdminGuard]
    }, {
        path: 'edit',
        component: AdminEditComponent,
        canActivate: [AdminAuthGuard, AuthLoginAdminGuard]
    }, {
        path: 'result',
        component: AdminResultComponent
    }]
}];

//taken from angular.io
//Only call RouterModule.forRoot in the root AppRoutingModule (or the AppModule if 
//that's where you register top level application routes). In any other module, you 
//must call the RouterModule.forChild method to register additional routes.

@NgModule({
    imports: [
        RouterModule.forChild(secondaryRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class SecondaryRoutingModule {}