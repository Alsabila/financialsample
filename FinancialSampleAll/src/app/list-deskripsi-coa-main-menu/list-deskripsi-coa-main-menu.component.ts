import {
    Component,
    OnInit
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';
import {
    ApiChartOfAccountListAbsService
} from '../services/api-chart-of-account-list-abs.service';

import * as introJs from 'intro.js/intro.js';
import {
    LearningToolSingleton
} from '@learning-tools/learning-tool';

@Component({
    selector: 'list-deskripsi-coa-main-menu',
    templateUrl: './list-deskripsi-coa-main-menu.component.html',
    styleUrls: ['./list-deskripsi-coa-main-menu.component.css']
})
export class ListDeskripsiCoaMainMenuComponent implements OnInit {


    instance = LearningToolSingleton.getInstance()
    introFeature = introJs();

    constructor(private route: ActivatedRoute, private router: Router, public apichartofaccountlistabsservice: ApiChartOfAccountListAbsService) {
        if (localStorage.getItem("Learning Tool Status") == "Done") {
            this.instance.setShowIntroJS(false);
            this.instance.setShowLearningTools(false);
        } else {
            this.setLearningTool()
        }
    }

    setLearningTool() {
        this.introFeature.setOptions(this.instance.getExplanationTooltip());
        this.introFeature.setOptions(this.instance.getFeatureExplanationIntroJSText());

        if (this.instance.getFeatureExplanationStatus() == "Finished") {
            this.instance.writeExplanationInLocalStorage();
            this.introFeature.oncomplete(() => {
                this.instance.setShowIntroJS(false);
                this.instance.getAssessmentResult();
            })
        }
    }

    ngOnInit() {
        if (this.instance.showIntroJS()) {
            this.introFeature.start();
        }

    }

    daftarkodeakun() {
        this.apichartofaccountlistabsservice.call().then(data => {
            this.router.navigate(['/halaman-daftar-akun'], {
                queryParams: {
                    jsonAllCoa: JSON.stringify(data['data'])
                }
            });
        }).catch(error => {
            this.router.navigate(['/halaman-daftar-akun'], {
                queryParams: {
                    jsonAllCoa: JSON.stringify([])
                }
            });
        });
    }

}