import {
    Component,
    OnInit,
    Input
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';
import {
    Expense
} from '../models/expense.model';
import {
    ApiExpenseDeleteAbsService
} from '../services/api-expense-delete-abs.service';

import * as introJs from 'intro.js/intro.js';
import {
    LearningToolSingleton
} from '@learning-tools/learning-tool';

@Component({
    selector: 'detail-expense',
    templateUrl: './detail-expense.component.html',
    styleUrls: ['./detail-expense.component.css']
})
export class DetailExpenseComponent implements OnInit {

    @Input() objectDetailExpense: any;
    @Input() idExpense: number;
    public expenseData: Expense;
    instance = LearningToolSingleton.getInstance()
    introFeature = introJs();

    constructor(private route: ActivatedRoute, private router: Router, public apiexpensedeleteabsservice: ApiExpenseDeleteAbsService) {
        if (localStorage.getItem("Learning Tool Status") == "Done") {
            this.instance.setShowIntroJS(false);
            this.instance.setShowLearningTools(false);
        } else {
            this.setLearningTool()
        }
    }

    setLearningTool() {
        this.introFeature.setOptions(this.instance.getExplanationTooltip());
        this.introFeature.setOptions(this.instance.getFeatureExplanationIntroJSText());

        if (this.instance.getFeatureExplanationStatus() == "Finished") {
            this.instance.writeExplanationInLocalStorage();
            this.introFeature.oncomplete(() => {
                this.instance.setShowIntroJS(false);
                this.instance.getAssessmentResult();
            })
        }
    }

    ngOnInit() {
        if (this.instance.showIntroJS()) {
            this.introFeature.start();
        }
        this.attachexpensedata();
    }

    attachexpensedata() {
        this.expenseData = this.objectDetailExpense;
        this.idExpense = this.expenseData.id;
    }
    hapus() {
        this.apiexpensedeleteabsservice.call({
            id: this.idExpense
        }).then(data => {
            this.router.navigate(['/catatan-pengeluaran'], {
                queryParams: {
                    jsonAllExpense: JSON.stringify(data['data'])
                }
            });
        }).catch(error => {
            this.router.navigate(['/catatan-pengeluaran'], {
                queryParams: {
                    jsonAllExpense: JSON.stringify([])
                }
            });
        });
    }

}