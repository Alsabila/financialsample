import {
    Component,
    OnInit,
    Input
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';
import {
    Expense
} from '../models/expense.model';
import {
    ApiExpenseDetailAbsService
} from '../services/api-expense-detail-abs.service';

@Component({
    selector: '[list-expense-content]',
    templateUrl: './list-expense-content.component.html',
    styleUrls: ['./list-expense-content.component.css']
})
export class ListExpenseContentComponent implements OnInit {

    @Input() jsonAllExpense: any;
    public idExpense: number;
    public expenseListElement: Expense;

    constructor(private route: ActivatedRoute, private router: Router, public apiexpensedetailabsservice: ApiExpenseDetailAbsService) {

    }

    ngOnInit() {
        this.attachexpenselistelement();
    }

    currencyFormatDE(num) {
        if (num == null) {
            return "0,00"
        } else {
            return (
                num
                .toFixed(2) // always two decimal digits
                .replace('.', ',') // replace decimal point character with ,
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
            ) // use . as a separator
        }
    }

    attachexpenselistelement() {
        this.expenseListElement = new Expense(this.jsonAllExpense);
        this.idExpense = this.expenseListElement.id;
        this.expenseListElement.amount = this.currencyFormatDE(this.expenseListElement.amount);
    }
    onclick() {
        this.apiexpensedetailabsservice.call({
            id: this.idExpense
        }).then(data => {
            this.router.navigate(['/halaman-detail-pengeluaran'], {
                queryParams: {
                    objectDetailExpense: JSON.stringify(data['data'])
                }
            });
        }).catch(error => {
            this.router.navigate(['/halaman-detail-pengeluaran'], {
                queryParams: {
                    objectDetailExpense: JSON.stringify([])
                }
            });
        });
    }

}