import {
    Component,
    OnInit,
    Input
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';
import {
    Income
} from '../models/income.model';
import {
    ApiIncomeDeleteAbsService
} from '../services/api-income-delete-abs.service';

import * as introJs from 'intro.js/intro.js';
import {
    LearningToolSingleton
} from '@learning-tools/learning-tool';

@Component({
    selector: 'detail-income',
    templateUrl: './detail-income.component.html',
    styleUrls: ['./detail-income.component.css']
})
export class DetailIncomeComponent implements OnInit {

    @Input() objectDetailIncome: any;
    @Input() idIncome: number;
    public incomeData: Income;
    instance = LearningToolSingleton.getInstance()
    introFeature = introJs();

    constructor(private route: ActivatedRoute, private router: Router, public apiincomedeleteabsservice: ApiIncomeDeleteAbsService) {
        if (localStorage.getItem("Learning Tool Status") == "Done") {
            this.instance.setShowIntroJS(false);
            this.instance.setShowLearningTools(false);
        } else {
            this.setLearningTool()
        }
    }

    setLearningTool() {
        this.introFeature.setOptions(this.instance.getExplanationTooltip());
        this.introFeature.setOptions(this.instance.getFeatureExplanationIntroJSText());

        if (this.instance.getFeatureExplanationStatus() == "Finished") {
            this.instance.writeExplanationInLocalStorage();
            this.introFeature.oncomplete(() => {
                this.instance.setShowIntroJS(false);
                this.instance.getAssessmentResult();
            })
        }
    }

    ngOnInit() {
        if (this.instance.showIntroJS()) {
            this.introFeature.start();
        }
        this.attachincomedata();
    }

    attachincomedata() {
        this.incomeData = this.objectDetailIncome;
        this.idIncome = this.incomeData.id;
    }
    delete() {
        this.apiincomedeleteabsservice.call({
            id: this.idIncome
        }).then(data => {
            this.router.navigate(['/catatan-pemasukan'], {
                queryParams: {
                    jsonAllIncome: JSON.stringify(data['data'])
                }
            });
        }).catch(error => {
            this.router.navigate(['/catatan-pemasukan'], {
                queryParams: {
                    jsonAllIncome: JSON.stringify([])
                }
            });
        });
    }

}