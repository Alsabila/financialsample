import {
    Component,
    OnInit
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';
import {
    ApiExpenseListAbsService
} from '../services/api-expense-list-abs.service';

import * as introJs from 'intro.js/intro.js';
import {
    LearningToolSingleton
} from '@learning-tools/learning-tool';

@Component({
    selector: 'expense-main-menu',
    templateUrl: './expense-main-menu.component.html',
    styleUrls: ['./expense-main-menu.component.css']
})
export class ExpenseMainMenuComponent implements OnInit {


    instance = LearningToolSingleton.getInstance()
    introFeature = introJs();

    constructor(private route: ActivatedRoute, private router: Router, public apiexpenselistabsservice: ApiExpenseListAbsService) {
        if (localStorage.getItem("Learning Tool Status") == "Done") {
            this.instance.setShowIntroJS(false);
            this.instance.setShowLearningTools(false);
        } else {
            this.setLearningTool()
        }
    }

    setLearningTool() {
        this.introFeature.setOptions(this.instance.getExplanationTooltip());
        this.introFeature.setOptions(this.instance.getFeatureExplanationIntroJSText());

        if (this.instance.getFeatureExplanationStatus() == "Finished") {
            this.instance.writeExplanationInLocalStorage();
            this.introFeature.oncomplete(() => {
                this.instance.setShowIntroJS(false);
                this.instance.getAssessmentResult();
            })
        }
    }

    ngOnInit() {
        if (this.instance.showIntroJS()) {
            this.introFeature.start();
        }

    }

    catatanpengeluaran() {
        this.apiexpenselistabsservice.call().then(data => {
            this.router.navigate(['/catatan-pengeluaran'], {
                queryParams: {
                    jsonAllExpense: JSON.stringify(data['data'])
                }
            });
        }).catch(error => {
            this.router.navigate(['/catatan-pengeluaran'], {
                queryParams: {
                    jsonAllExpense: JSON.stringify([])
                }
            });
        });
    }

}