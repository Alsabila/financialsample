import {
    NgModule
} from '@angular/core';
import {
    Routes,
    RouterModule
} from '@angular/router';
import {
    ListDeskripsiCoaMainMenuComponent
} from './list-deskripsi-coa-main-menu/list-deskripsi-coa-main-menu.component';
import {
    DaftarKodeAkunComponent
} from './daftar-kode-akun/daftar-kode-akun.component';
import {
    HalamanDaftarAkunComponent
} from './halaman-daftar-akun/halaman-daftar-akun.component';
import {
    IncomeMainMenuComponent
} from './income-main-menu/income-main-menu.component';
import {
    IncomeMenuFeatureComponent
} from './income-menu-feature/income-menu-feature.component';
import {
    ListIncomeContentComponent
} from './list-income-content/list-income-content.component';
import {
    CatatanPemasukanComponent
} from './catatan-pemasukan/catatan-pemasukan.component';
import {
    TambahkanPemasukanComponent
} from './tambahkan-pemasukan/tambahkan-pemasukan.component';
import {
    HalamanTambahPemasukanComponent
} from './halaman-tambah-pemasukan/halaman-tambah-pemasukan.component';
import {
    DetailIncomeComponent
} from './detail-income/detail-income.component';
import {
    HalamanDetailPemasukanComponent
} from './halaman-detail-pemasukan/halaman-detail-pemasukan.component';
import {
    ExpenseMainMenuComponent
} from './expense-main-menu/expense-main-menu.component';
import {
    ExpenseFeatureComponent
} from './expense-feature/expense-feature.component';
import {
    ListExpenseContentComponent
} from './list-expense-content/list-expense-content.component';
import {
    CatatanPengeluaranComponent
} from './catatan-pengeluaran/catatan-pengeluaran.component';
import {
    TambahkanPengeluaranComponent
} from './tambahkan-pengeluaran/tambahkan-pengeluaran.component';
import {
    HalamanTambahPengeluaranComponent
} from './halaman-tambah-pengeluaran/halaman-tambah-pengeluaran.component';
import {
    DetailExpenseComponent
} from './detail-expense/detail-expense.component';
import {
    HalamanDetailPengeluaranComponent
} from './halaman-detail-pengeluaran/halaman-detail-pengeluaran.component';
import {
    CoaMainMenuComponent
} from './coa-main-menu/coa-main-menu.component';
import {
    ListChartOfAccountEntryComponent
} from './list-chart-of-account-entry/list-chart-of-account-entry.component';
import {
    LaporanKeuanganComponent
} from './laporan-keuangan/laporan-keuangan.component';
import {
    ProgramMainMenuComponent
} from './program-main-menu/program-main-menu.component';
import {
    ProgramFeatureComponent
} from './program-feature/program-feature.component';
import {
    ListProgramContentComponent
} from './list-program-content/list-program-content.component';
import {
    DaftarProgramComponent
} from './daftar-program/daftar-program.component';
import {
    TambahkanProgramComponent
} from './tambahkan-program/tambahkan-program.component';
import {
    HalamanTambahProgramComponent
} from './halaman-tambah-program/halaman-tambah-program.component';
import {
    DetailProgramComponent
} from './detail-program/detail-program.component';
import {
    HalamanDetailProgramComponent
} from './halaman-detail-program/halaman-detail-program.component';
import {
    UbahDataProgramComponent
} from './ubah-data-program/ubah-data-program.component';
import {
    HalamanUbahProgramComponent
} from './halaman-ubah-program/halaman-ubah-program.component';
import {
    SummaryMainMenuComponent
} from './summary-main-menu/summary-main-menu.component';
import {
    ListSummaryContentComponent
} from './list-summary-content/list-summary-content.component';
import {
    CatatanTransaksiComponent
} from './catatan-transaksi/catatan-transaksi.component';
import {
    DetailSummaryComponent
} from './detail-summary/detail-summary.component';
import {
    HalamanDetailTransaksiComponent
} from './halaman-detail-transaksi/halaman-detail-transaksi.component';
import {
    MainPageComponent
} from './main-page/main-page.component';
import {
    LoginComponent
} from './login/login.component';
import {
    ErrorPageNotLoginComponent
} from './error-page-not-login/error-page-not-login.component';
import {
    ErrorPageNoPermissionComponent
} from './error-page-no-permission/error-page-no-permission.component';
import {
    HomepageComponent
} from './homepage/homepage.component';
import {
    AboutComponent
} from './about/about.component';
import {
    ContactComponent
} from './contact/contact.component';
import {
    OauthRedirectComponent
} from './oauth-redirect/oauth-redirect.component';
import {
    AuthGuard
} from './guard/auth.guard';
import {
    AdminAuthGuard
} from './guard/auth-admin.guard';
import {
    StaffAuthGuard
} from './guard/auth-staff.guard';
import {
    DonorAuthGuard
} from './guard/auth-donor.guard';
import {
    RegisteredAuthGuard
} from './guard/auth-registered.guard';

export const routes: Routes = [{
    path: '',
    component: MainPageComponent,
    children: [{
        path: '',
        redirectTo: 'homepage',
        pathMatch: 'full'
    }, {
        path: 'halaman-daftar-akun',
        component: HalamanDaftarAkunComponent,
        canActivate: []
    }, {
        path: 'catatan-pemasukan',
        component: CatatanPemasukanComponent,
        canActivate: [],
        children: []
    }, {
        path: 'halaman-tambah-pemasukan',
        component: HalamanTambahPemasukanComponent,
        canActivate: [AdminAuthGuard, AuthGuard, ]
    }, {
        path: 'halaman-detail-pemasukan',
        component: HalamanDetailPemasukanComponent,
        canActivate: [AdminAuthGuard, AuthGuard, ]
    }, {
        path: 'catatan-pengeluaran',
        component: CatatanPengeluaranComponent,
        canActivate: [],
        children: []
    }, {
        path: 'halaman-tambah-pengeluaran',
        component: HalamanTambahPengeluaranComponent,
        canActivate: [AdminAuthGuard, AuthGuard, ]
    }, {
        path: 'halaman-detail-pengeluaran',
        component: HalamanDetailPengeluaranComponent,
        canActivate: [AdminAuthGuard, AuthGuard, ]
    }, {
        path: 'laporan-keuangan',
        component: LaporanKeuanganComponent,
        canActivate: []
    }, {
        path: 'daftar-program',
        component: DaftarProgramComponent,
        canActivate: [],
        children: []
    }, {
        path: 'halaman-tambah-program',
        component: HalamanTambahProgramComponent,
        canActivate: [AdminAuthGuard, AuthGuard, ]
    }, {
        path: 'halaman-detail-program',
        component: HalamanDetailProgramComponent,
        canActivate: [AdminAuthGuard, AuthGuard, ]
    }, {
        path: 'halaman-ubah-program',
        component: HalamanUbahProgramComponent,
        canActivate: []
    }, {
        path: 'catatan-transaksi',
        component: CatatanTransaksiComponent,
        canActivate: [],
        children: []
    }, {
        path: 'halaman-detail-transaksi',
        component: HalamanDetailTransaksiComponent,
        canActivate: [AdminAuthGuard, AuthGuard, ]
    }, {
        path: 'login',
        component: LoginComponent
    }, {
        path: 'error-page-not-login',
        component: ErrorPageNotLoginComponent
    }, {
        path: 'error-page-no-permission',
        component: ErrorPageNoPermissionComponent
    }, {
        path: 'homepage',
        component: HomepageComponent
    }, {
        path: 'about',
        component: AboutComponent
    }, {
        path: 'contact',
        component: ContactComponent
    }, {
        path: 'oauth-redirect',
        component: OauthRedirectComponent
    }]
}];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}