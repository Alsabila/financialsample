import {
    BrowserModule
} from '@angular/platform-browser';
import {
    NgModule
} from '@angular/core';
import {
    AppRoutingModule
} from './app-routing.module';
import {
    AppComponent
} from './app.component';
import {
    ServiceWorkerModule
} from '@angular/service-worker';
import {
    environment
} from '../environments/environment';
import {
    HttpClientModule
} from '@angular/common/http';
import {
    FormsModule
} from '@angular/forms';
import {
    NgxSmartModalModule,
    NgxSmartModalService
} from 'ngx-smart-modal';
import {
    FilterPipe
} from './filter.pipe';
import {
    SecondaryModule
} from './secondary.module';
import {
    ListDeskripsiCoaMainMenuComponent
} from './list-deskripsi-coa-main-menu/list-deskripsi-coa-main-menu.component';
import {
    DaftarKodeAkunComponent
} from './daftar-kode-akun/daftar-kode-akun.component';
import {
    HalamanDaftarAkunComponent
} from './halaman-daftar-akun/halaman-daftar-akun.component';
import {
    IncomeMainMenuComponent
} from './income-main-menu/income-main-menu.component';
import {
    IncomeMenuFeatureComponent
} from './income-menu-feature/income-menu-feature.component';
import {
    ListIncomeContentComponent
} from './list-income-content/list-income-content.component';
import {
    CatatanPemasukanComponent
} from './catatan-pemasukan/catatan-pemasukan.component';
import {
    TambahkanPemasukanComponent
} from './tambahkan-pemasukan/tambahkan-pemasukan.component';
import {
    HalamanTambahPemasukanComponent
} from './halaman-tambah-pemasukan/halaman-tambah-pemasukan.component';
import {
    DetailIncomeComponent
} from './detail-income/detail-income.component';
import {
    HalamanDetailPemasukanComponent
} from './halaman-detail-pemasukan/halaman-detail-pemasukan.component';
import {
    ExpenseMainMenuComponent
} from './expense-main-menu/expense-main-menu.component';
import {
    ExpenseFeatureComponent
} from './expense-feature/expense-feature.component';
import {
    ListExpenseContentComponent
} from './list-expense-content/list-expense-content.component';
import {
    CatatanPengeluaranComponent
} from './catatan-pengeluaran/catatan-pengeluaran.component';
import {
    TambahkanPengeluaranComponent
} from './tambahkan-pengeluaran/tambahkan-pengeluaran.component';
import {
    HalamanTambahPengeluaranComponent
} from './halaman-tambah-pengeluaran/halaman-tambah-pengeluaran.component';
import {
    DetailExpenseComponent
} from './detail-expense/detail-expense.component';
import {
    HalamanDetailPengeluaranComponent
} from './halaman-detail-pengeluaran/halaman-detail-pengeluaran.component';
import {
    CoaMainMenuComponent
} from './coa-main-menu/coa-main-menu.component';
import {
    ListChartOfAccountEntryComponent
} from './list-chart-of-account-entry/list-chart-of-account-entry.component';
import {
    LaporanKeuanganComponent
} from './laporan-keuangan/laporan-keuangan.component';
import {
    ProgramMainMenuComponent
} from './program-main-menu/program-main-menu.component';
import {
    ProgramFeatureComponent
} from './program-feature/program-feature.component';
import {
    ListProgramContentComponent
} from './list-program-content/list-program-content.component';
import {
    DaftarProgramComponent
} from './daftar-program/daftar-program.component';
import {
    TambahkanProgramComponent
} from './tambahkan-program/tambahkan-program.component';
import {
    HalamanTambahProgramComponent
} from './halaman-tambah-program/halaman-tambah-program.component';
import {
    DetailProgramComponent
} from './detail-program/detail-program.component';
import {
    HalamanDetailProgramComponent
} from './halaman-detail-program/halaman-detail-program.component';
import {
    UbahDataProgramComponent
} from './ubah-data-program/ubah-data-program.component';
import {
    HalamanUbahProgramComponent
} from './halaman-ubah-program/halaman-ubah-program.component';
import {
    SummaryMainMenuComponent
} from './summary-main-menu/summary-main-menu.component';
import {
    ListSummaryContentComponent
} from './list-summary-content/list-summary-content.component';
import {
    CatatanTransaksiComponent
} from './catatan-transaksi/catatan-transaksi.component';
import {
    DetailSummaryComponent
} from './detail-summary/detail-summary.component';
import {
    HalamanDetailTransaksiComponent
} from './halaman-detail-transaksi/halaman-detail-transaksi.component';
import {
    MainPageComponent
} from './main-page/main-page.component';
import {
    LoginComponent
} from './login/login.component';
import {
    ErrorPageNotLoginComponent
} from './error-page-not-login/error-page-not-login.component';
import {
    ErrorPageNoPermissionComponent
} from './error-page-no-permission/error-page-no-permission.component';
import {
    HomepageComponent
} from './homepage/homepage.component';
import {
    AboutComponent
} from './about/about.component';
import {
    ContactComponent
} from './contact/contact.component';
import {
    OauthRedirectComponent
} from './oauth-redirect/oauth-redirect.component';

@NgModule({
    declarations: [
        AppComponent,
        FilterPipe,
        ListDeskripsiCoaMainMenuComponent,
        DaftarKodeAkunComponent,
        HalamanDaftarAkunComponent,
        IncomeMainMenuComponent,
        IncomeMenuFeatureComponent,
        ListIncomeContentComponent,
        CatatanPemasukanComponent,
        TambahkanPemasukanComponent,
        HalamanTambahPemasukanComponent,
        DetailIncomeComponent,
        HalamanDetailPemasukanComponent,
        ExpenseMainMenuComponent,
        ExpenseFeatureComponent,
        ListExpenseContentComponent,
        CatatanPengeluaranComponent,
        TambahkanPengeluaranComponent,
        HalamanTambahPengeluaranComponent,
        DetailExpenseComponent,
        HalamanDetailPengeluaranComponent,
        CoaMainMenuComponent,
        ListChartOfAccountEntryComponent,
        LaporanKeuanganComponent,
        ProgramMainMenuComponent,
        ProgramFeatureComponent,
        ListProgramContentComponent,
        DaftarProgramComponent,
        TambahkanProgramComponent,
        HalamanTambahProgramComponent,
        DetailProgramComponent,
        HalamanDetailProgramComponent,
        UbahDataProgramComponent,
        HalamanUbahProgramComponent,
        SummaryMainMenuComponent,
        ListSummaryContentComponent,
        CatatanTransaksiComponent,
        DetailSummaryComponent,
        HalamanDetailTransaksiComponent,
        MainPageComponent,
        LoginComponent,
        ErrorPageNotLoginComponent,
        ErrorPageNoPermissionComponent,
        HomepageComponent,
        AboutComponent,
        ContactComponent,
        OauthRedirectComponent,
        LoginComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        NgxSmartModalModule.forRoot(),
        AppRoutingModule,
        FormsModule,
        SecondaryModule,
        ServiceWorkerModule.register('ngsw-worker.js', {
            enabled: environment.production
        })
    ],
    providers: [NgxSmartModalService],
    bootstrap: [AppComponent],
    // Masih tambah entryComponents manual
    entryComponents: [ProgramMainMenuComponent, IncomeMainMenuComponent, ExpenseMainMenuComponent, SummaryMainMenuComponent, CoaMainMenuComponent]
})
export class AppModule {}