import {
    Component,
    OnInit,
    Input
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';
import {
    Summary
} from '../models/summary.model';
import {
    ApiSummaryDetailAbsService
} from '../services/api-summary-detail-abs.service';

@Component({
    selector: '[list-summary-content]',
    templateUrl: './list-summary-content.component.html',
    styleUrls: ['./list-summary-content.component.css']
})
export class ListSummaryContentComponent implements OnInit {

    @Input() jsonAllSummary: any;
    public idSummary: number;
    public summaryListElement: Summary;

    constructor(private route: ActivatedRoute, private router: Router, public apisummarydetailabsservice: ApiSummaryDetailAbsService) {

    }

    ngOnInit() {
        this.attachsummarylistelement();
    }

    currencyFormatDE(num) {
        if (num == null) {
            return "0,00"
        } else {
            return (
                num
                .toFixed(2) // always two decimal digits
                .replace('.', ',') // replace decimal point character with ,
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
            ) // use . as a separator
        }
    }

    attachsummarylistelement() {
        this.summaryListElement = new Summary(this.jsonAllSummary);
        this.idSummary = this.summaryListElement.id;
        this.summaryListElement.income = this.currencyFormatDE(this.summaryListElement.income);
        this.summaryListElement.expense = this.currencyFormatDE(this.summaryListElement.expense);
    }
    onclick() {
        this.apisummarydetailabsservice.call({
            id: this.idSummary
        }).then(data => {
            this.router.navigate(['/halaman-detail-transaksi'], {
                queryParams: {
                    objectDetailSummary: JSON.stringify(data['data'])
                }
            });
        }).catch(error => {
            this.router.navigate(['/halaman-detail-transaksi'], {
                queryParams: {
                    objectDetailSummary: JSON.stringify([])
                }
            });
        });
    }

}