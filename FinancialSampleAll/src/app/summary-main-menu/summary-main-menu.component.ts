import {
    Component,
    OnInit
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';
import {
    ApiSummaryListAbsService
} from '../services/api-summary-list-abs.service';

import * as introJs from 'intro.js/intro.js';
import {
    LearningToolSingleton
} from '@learning-tools/learning-tool';

@Component({
    selector: 'summary-main-menu',
    templateUrl: './summary-main-menu.component.html',
    styleUrls: ['./summary-main-menu.component.css']
})
export class SummaryMainMenuComponent implements OnInit {


    instance = LearningToolSingleton.getInstance()
    introFeature = introJs();

    constructor(private route: ActivatedRoute, private router: Router, public apisummarylistabsservice: ApiSummaryListAbsService) {
        if (localStorage.getItem("Learning Tool Status") == "Done") {
            this.instance.setShowIntroJS(false);
            this.instance.setShowLearningTools(false);
        } else {
            this.setLearningTool()
        }
    }

    setLearningTool() {
        this.introFeature.setOptions(this.instance.getExplanationTooltip());
        this.introFeature.setOptions(this.instance.getFeatureExplanationIntroJSText());

        if (this.instance.getFeatureExplanationStatus() == "Finished") {
            this.instance.writeExplanationInLocalStorage();
            this.introFeature.oncomplete(() => {
                this.instance.setShowIntroJS(false);
                this.instance.getAssessmentResult();
            })
        }
    }

    ngOnInit() {
        if (this.instance.showIntroJS()) {
            this.introFeature.start();
        }

    }

    catatantransaksi() {
        this.apisummarylistabsservice.call().then(data => {
            this.router.navigate(['/catatan-transaksi'], {
                queryParams: {
                    jsonAllSummary: JSON.stringify(data['data'])
                }
            });
        }).catch(error => {
            this.router.navigate(['/catatan-transaksi'], {
                queryParams: {
                    jsonAllSummary: JSON.stringify([])
                }
            });
        });
    }

}