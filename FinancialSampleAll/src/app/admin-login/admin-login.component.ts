import {
    Component,
    OnInit
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';

@Component({
    selector: 'app-admin-login',
    templateUrl: './admin-login.component.html',
    styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {

    public isLoggedIn = false;

    constructor(private route: ActivatedRoute, private router: Router) {}

    ngOnInit() {
        if (localStorage.getItem('token')) {
            this.isLoggedIn = true;
        } else {
            this.isLoggedIn = false;
        }
    }

    submitLogin() {
        window.location.replace("https://" + "ichlaffterlalu.au.auth0.com" + "/authorize?response_type=id_token&client_id=" + "ED4753GKzpgdvb7sz2pSm9RwwovPTU3q" + "&audience=https://*." + "splelive.id" + "&redirect_uri=" + "http://34.101.241.250" + "/oauth-redirect&scope=openid%20email%20profile&nonce=nduqwihuc78rqwjnAhihniewhfnygr" + "&state=a");
    }

    public logout() {
        localStorage.removeItem('token');
        localStorage.removeItem('email');
        location.reload();
    }

}