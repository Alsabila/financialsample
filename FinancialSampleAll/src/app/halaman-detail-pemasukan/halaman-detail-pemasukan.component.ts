import {
    Component,
    OnInit
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';

import * as introJs from 'intro.js/intro.js';
import {
    LearningToolSingleton
} from '@learning-tools/learning-tool';

@Component({
    selector: 'halaman-detail-pemasukan',
    templateUrl: './halaman-detail-pemasukan.component.html',
    styleUrls: ['./halaman-detail-pemasukan.component.css']
})
export class HalamanDetailPemasukanComponent implements OnInit {

    public objectDetailIncome: any;
    public searchText: string
    public idIncome: number;
    instance = LearningToolSingleton.getInstance()
    introFeature = introJs();

    constructor(private route: ActivatedRoute, private router: Router) {
        if (localStorage.getItem("Learning Tool Status") == "Done") {
            this.instance.setShowIntroJS(false);
            this.instance.setShowLearningTools(false);
        } else {
            this.setLearningTool()
        }
    }

    setLearningTool() {
        this.introFeature.setOptions(this.instance.getExplanationTooltip());
        this.introFeature.setOptions(this.instance.getFeatureExplanationIntroJSText());

        if (this.instance.getFeatureExplanationStatus() == "Finished") {
            this.instance.writeExplanationInLocalStorage();
            this.introFeature.oncomplete(() => {
                this.instance.setShowIntroJS(false);
                this.instance.getAssessmentResult();
            })
        }
    }

    ngOnInit() {
        if (this.instance.showIntroJS()) {
            this.introFeature.start();
        }
        this.route.queryParams.subscribe(params => {
            this.objectDetailIncome = JSON.parse(params.objectDetailIncome);
            this.idIncome = JSON.parse(params.idIncome);
        });
    }



}