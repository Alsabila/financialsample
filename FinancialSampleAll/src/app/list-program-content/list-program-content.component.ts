import {
    Component,
    OnInit,
    Input
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';
import {
    Program
} from '../models/program.model';
import {
    ApiProgramDetailAbsService
} from '../services/api-program-detail-abs.service';

import * as introJs from 'intro.js/intro.js';
import {
    LearningToolSingleton
} from '@learning-tools/learning-tool';

@Component({
    selector: 'list-program-content',
    templateUrl: './list-program-content.component.html',
    styleUrls: ['./list-program-content.component.css']
})
export class ListProgramContentComponent implements OnInit {

    @Input() jsonAllProgram: any;
    public id: number;
    public programListElement: Program;
    instance = LearningToolSingleton.getInstance()
    introFeature = introJs();

    constructor(private route: ActivatedRoute, private router: Router, public apiprogramdetailabsservice: ApiProgramDetailAbsService) {
        if (localStorage.getItem("Learning Tool Status") == "Done") {
            this.instance.setShowIntroJS(false);
            this.instance.setShowLearningTools(false);
        } else {
            this.setLearningTool()
        }
    }

    setLearningTool() {
        this.introFeature.setOptions(this.instance.getExplanationTooltip());
        this.introFeature.setOptions(this.instance.getFeatureExplanationIntroJSText());

        if (this.instance.getFeatureExplanationStatus() == "Finished") {
            this.instance.writeExplanationInLocalStorage();
            this.introFeature.oncomplete(() => {
                this.instance.setShowIntroJS(false);
                this.instance.getAssessmentResult();
            })
        }
    }

    ngOnInit() {
        if (this.instance.showIntroJS()) {
            this.introFeature.start();
        }
        this.attachprogramlistelement();
    }

    attachprogramlistelement() {
        this.programListElement = new Program(this.jsonAllProgram);
        this.id = this.programListElement.id;
    }
    onclick() {
        this.apiprogramdetailabsservice.call({
            id: this.id
        }).then(data => {
            this.router.navigate(['/halaman-detail-program'], {
                queryParams: {
                    objectDetailProgram: JSON.stringify(data['data'])
                }
            });
        }).catch(error => {
            this.router.navigate(['/halaman-detail-program'], {
                queryParams: {
                    objectDetailProgram: JSON.stringify([])
                }
            });
        });
    }

}