import {
    Component,
    OnInit
} from '@angular/core';

import {
    ActivatedRoute,
    Router
} from '@angular/router';

@Component({
    selector: 'app-error-page-not-login',
    templateUrl: './error-page-not-login.component.html',
    styleUrls: ['./error-page-not-login.component.css']
})
export class ErrorPageNotLoginComponent implements OnInit {

    constructor(private route: ActivatedRoute, private router: Router) {}

    public isLoggedIn = false;

    ngOnInit() {
        if (localStorage.getItem('token')) {
            this.isLoggedIn = true;
        } else {
            this.isLoggedIn = false;
        }
    }

    submitLogin() {
        window.location.replace("https://" + "ichlaffterlalu.au.auth0.com" + "/authorize?response_type=id_token&client_id=" + "ED4753GKzpgdvb7sz2pSm9RwwovPTU3q" + "&audience=https://*." + "splelive.id" + "&redirect_uri=" + "http://34.101.241.250" + "/oauth-redirect&scope=openid%20email%20profile&nonce=nduqwihuc78rqwjnAhihniewhfnygr" + "&state=h");
    }

    public logout() {
        localStorage.removeItem('token');
        localStorage.removeItem('email');
        location.reload();
    }

}