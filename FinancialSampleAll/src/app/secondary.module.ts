//this is modelled from your app.module.ts and the components and 
//services are just arbitrary examples, your module might be different
import {
    NgModule
} from '@angular/core';
import {
    CommonModule
} from '@angular/common';
import {
    FormsModule
} from '@angular/forms';

import {
    AdminMainComponent
} from './admin-main/admin-main.component'
import {
    AdminLoginComponent
} from './admin-login/admin-login.component';
import {
    AdminEditComponent
} from './admin-edit/admin-edit.component';
import {
    AdminResultComponent
} from './admin-result/admin-result.component';

import {
    SecondaryRoutingModule
} from './secondary-routing.module' //<-- import

@NgModule({
    imports: [
        CommonModule,
        SecondaryRoutingModule,
        FormsModule
    ],
    declarations: [
        AdminMainComponent,
        AdminLoginComponent,
        AdminEditComponent,
        AdminResultComponent
    ]
})
export class SecondaryModule {}