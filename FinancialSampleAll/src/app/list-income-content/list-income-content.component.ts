import {
    Component,
    OnInit,
    Input
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';
import {
    Income
} from '../models/income.model';
import {
    ApiIncomeDetailAbsService
} from '../services/api-income-detail-abs.service';

@Component({
    selector: '[list-income-content]',
    templateUrl: './list-income-content.component.html',
    styleUrls: ['./list-income-content.component.css']
})
export class ListIncomeContentComponent implements OnInit {

    @Input() jsonAllIncome: any;
    public idIncome: number;
    public incomeListElement: Income;

    constructor(private route: ActivatedRoute, private router: Router, public apiincomedetailabsservice: ApiIncomeDetailAbsService) {

    }

    ngOnInit() {
        this.attachincomelistelement();
    }

    currencyFormatDE(num) {
        if (num == null) {
            return "0,00"
        } else {
            return (
                num
                .toFixed(2) // always two decimal digits
                .replace('.', ',') // replace decimal point character with ,
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
            ) // use . as a separator
        }
    }

    attachincomelistelement() {
        this.incomeListElement = new Income(this.jsonAllIncome);
        this.idIncome = this.incomeListElement.id;
        this.incomeListElement.amount = this.currencyFormatDE(this.incomeListElement.amount);
    }
    onclick() {
        this.apiincomedetailabsservice.call({
            id: this.idIncome
        }).then(data => {
            this.router.navigate(['/halaman-detail-pemasukan'], {
                queryParams: {
                    objectDetailIncome: JSON.stringify(data['data'])
                }
            });
        }).catch(error => {
            this.router.navigate(['/halaman-detail-pemasukan'], {
                queryParams: {
                    objectDetailIncome: JSON.stringify([])
                }
            });
        });
    }

}