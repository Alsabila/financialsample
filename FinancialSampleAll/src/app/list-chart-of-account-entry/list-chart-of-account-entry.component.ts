import {
    Component,
    OnInit,
    Input
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';
import {
    ChartOfAccountEntry
} from '../models/chart-of-account-entry.model';

@Component({
    selector: '[list-chart-of-account-entry]',
    templateUrl: './list-chart-of-account-entry.component.html',
    styleUrls: ['./list-chart-of-account-entry.component.css']
})
export class ListChartOfAccountEntryComponent implements OnInit {

    @Input() jsonAllChartOfAccountEntry: any;
    public chartOfAccountEntryElement: ChartOfAccountEntry;

    constructor(private route: ActivatedRoute, private router: Router) {

    }

    ngOnInit() {
        this.attachchartofaccountentryelement();
    }

    currencyFormatDE(num) {
        if (num == null) {
            return "0,00"
        } else {
            return (
                num
                .toFixed(2) // always two decimal digits
                .replace('.', ',') // replace decimal point character with ,
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
            ) // use . as a separator
        }
    }

    attachchartofaccountentryelement() {
        this.chartOfAccountEntryElement = new ChartOfAccountEntry(this.jsonAllChartOfAccountEntry);
        this.chartOfAccountEntryElement.amount = this.currencyFormatDE(this.chartOfAccountEntryElement.amount);
    }

}