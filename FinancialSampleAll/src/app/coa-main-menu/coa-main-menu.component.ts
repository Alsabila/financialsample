import {
    Component,
    OnInit
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';
import {
    ApiAutomaticReportListAbsService
} from '../services/api-automatic-report-list-abs.service';

import * as introJs from 'intro.js/intro.js';
import {
    LearningToolSingleton
} from '@learning-tools/learning-tool';

@Component({
    selector: 'coa-main-menu',
    templateUrl: './coa-main-menu.component.html',
    styleUrls: ['./coa-main-menu.component.css']
})
export class CoaMainMenuComponent implements OnInit {


    instance = LearningToolSingleton.getInstance()
    introFeature = introJs();

    constructor(private route: ActivatedRoute, private router: Router, public apiautomaticreportlistabsservice: ApiAutomaticReportListAbsService) {
        if (localStorage.getItem("Learning Tool Status") == "Done") {
            this.instance.setShowIntroJS(false);
            this.instance.setShowLearningTools(false);
        } else {
            this.setLearningTool()
        }
    }

    setLearningTool() {
        this.introFeature.setOptions(this.instance.getExplanationTooltip());
        this.introFeature.setOptions(this.instance.getFeatureExplanationIntroJSText());

        if (this.instance.getFeatureExplanationStatus() == "Finished") {
            this.instance.writeExplanationInLocalStorage();
            this.introFeature.oncomplete(() => {
                this.instance.setShowIntroJS(false);
                this.instance.getAssessmentResult();
            })
        }
    }

    ngOnInit() {
        if (this.instance.showIntroJS()) {
            this.introFeature.start();
        }

    }

    laporankeuangan() {
        this.apiautomaticreportlistabsservice.call().then(data => {
            this.router.navigate(['/laporan-keuangan'], {
                queryParams: {
                    jsonAllChartOfAccountEntry: JSON.stringify(data['data'])
                }
            });
        }).catch(error => {
            this.router.navigate(['/laporan-keuangan'], {
                queryParams: {
                    jsonAllChartOfAccountEntry: JSON.stringify([])
                }
            });
        });
    }

}