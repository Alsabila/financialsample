export class Event {
    public id: number;
    public name: string;
    public description: string;
    public eventDate: string;
    public partner: string;
    public target: string;
    public logoUrl: string;

    constructor(obj ? : any) {
        this.id = obj && obj.id || null;
        this.name = obj && obj.name || null;
        this.description = obj && obj.description || null;
        this.eventDate = obj && obj.eventDate || null;
        this.partner = obj && obj.partner || null;
        this.target = obj && obj.target || null;
        this.logoUrl = obj && obj.logoUrl || null;
    }


}