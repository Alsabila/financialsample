export class Income {
    public id: number;
    public datestamp: Date;
    public description: string;
    public amount: number;
    public idCoa: string;
    public programName: string;

    constructor(obj ? : any) {
        this.id = obj && obj.id || null;
        this.datestamp = obj && obj.datestamp || null;
        this.description = obj && obj.description || null;
        this.amount = obj && obj.amount || null;
        this.idCoa = obj && obj.idCoa || null;
        this.programName = obj && obj.programName || null;
    }


}