import {
    Component,
    OnInit
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';

import * as introJs from 'intro.js/intro.js';
import {
    LearningToolSingleton
} from '@learning-tools/learning-tool';

@Component({
    selector: 'income-menu-feature',
    templateUrl: './income-menu-feature.component.html',
    styleUrls: ['./income-menu-feature.component.css']
})
export class IncomeMenuFeatureComponent implements OnInit {


    instance = LearningToolSingleton.getInstance()
    introFeature = introJs();

    constructor(private route: ActivatedRoute, private router: Router) {
        if (localStorage.getItem("Learning Tool Status") == "Done") {
            this.instance.setShowIntroJS(false);
            this.instance.setShowLearningTools(false);
        } else {
            this.setLearningTool()
        }
    }

    setLearningTool() {
        this.introFeature.setOptions(this.instance.getExplanationTooltip());
        this.introFeature.setOptions(this.instance.getFeatureExplanationIntroJSText());

        if (this.instance.getFeatureExplanationStatus() == "Finished") {
            this.instance.writeExplanationInLocalStorage();
            this.introFeature.oncomplete(() => {
                this.instance.setShowIntroJS(false);
                this.instance.getAssessmentResult();
            })
        }
    }

    ngOnInit() {
        if (this.instance.showIntroJS()) {
            this.introFeature.start();
        }

    }

    tambahpemasukan() {
        this.router.navigate(['/halaman-tambah-pemasukan']);
    }

}