import {
    Component,
    OnInit,
    Input
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';
import {
    FileUploadService
} from '../services/file-upload.service';
import {
    Program
} from '../models/program.model';
import {
    ApiProgramUpdateAbsService
} from '../services/api-program-update-abs.service';

import * as introJs from 'intro.js/intro.js';
import {
    LearningToolSingleton
} from '@learning-tools/learning-tool';

@Component({
    selector: 'ubah-data-program',
    templateUrl: './ubah-data-program.component.html',
    styleUrls: ['./ubah-data-program.component.css']
})
export class UbahDataProgramComponent implements OnInit {

    @Input() objectEditProgram: any;
    @Input() namaProgram: string;
    @Input() deskripsi: string;
    @Input() target: string;
    @Input() partner: string;
    @Input() urlGambarProgram: string;
    public fileToUpload: File;
    @Input() id: number;
    public programData: Program;
    instance = LearningToolSingleton.getInstance()
    introFeature = introJs();

    constructor(private route: ActivatedRoute, private router: Router, public fileUploadService: FileUploadService, public apiprogramupdateabsservice: ApiProgramUpdateAbsService) {
        if (localStorage.getItem("Learning Tool Status") == "Done") {
            this.instance.setShowIntroJS(false);
            this.instance.setShowLearningTools(false);
        } else {
            this.setLearningTool()
        }
    }

    setLearningTool() {
        this.introFeature.setOptions(this.instance.getExplanationTooltip());
        this.introFeature.setOptions(this.instance.getFeatureExplanationIntroJSText());

        if (this.instance.getFeatureExplanationStatus() == "Finished") {
            this.instance.writeExplanationInLocalStorage();
            this.introFeature.oncomplete(() => {
                this.instance.setShowIntroJS(false);
                this.instance.getAssessmentResult();
            })
        }
    }

    ngOnInit() {
        if (this.instance.showIntroJS()) {
            this.introFeature.start();
        }
        this.attachprogramdata();
    }

    handleFileInput(files: FileList) {
        this.fileToUpload = files.item(0);
        console.log('In..')
        console.log(this.fileToUpload)
        this.uploadFileToActivity();
    }

    resetCss() {
        var current = document.getElementsByClassName("done");
        if (current.length > 0) {
            current[0].className = current[0].className.replace(" done", "");
        }
    }

    uploadFileToActivity() {
        this.fileUploadService.postFile(this.fileToUpload).subscribe(data => {
            this.urlGambarProgram = data;
            var currentActive = document.getElementById("input-url-gambar-program");
            currentActive.className += " done";
        }, error => {
            console.log(error);
        });
    }
    attachprogramdata() {
        this.programData = this.objectEditProgram;
        this.id = this.programData.id;
        this.namaProgram = this.programData.name;
        this.deskripsi = this.programData.description;
        this.partner = this.programData.partner;
        this.target = this.programData.target;
        this.urlGambarProgram = this.programData.logoUrl;
    }
    kirim() {
        this.apiprogramupdateabsservice.call({
            name: this.namaProgram,
            description: this.deskripsi,
            target: this.target,
            partner: this.partner,
            logoUrl: this.urlGambarProgram,
            id: this.id
        }).then(data => {
            this.router.navigate(['/halaman-detail-program'], {
                queryParams: {
                    objectDetailProgram: JSON.stringify(data['data'])
                }
            });
        }).catch(error => {
            this.router.navigate(['/halaman-detail-program'], {
                queryParams: {
                    objectDetailProgram: JSON.stringify([])
                }
            });
        });
    }

}