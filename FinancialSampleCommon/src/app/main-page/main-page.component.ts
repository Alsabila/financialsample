import {
    Component,
    OnInit
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';
import {
    StaticPageService
} from '../services/static-page.service';
import {
    ViewChild,
    ViewContainerRef,
    ComponentFactoryResolver
} from '@angular/core';
import {
    IncomeMainMenuComponent
} from '../income-main-menu/income-main-menu.component';
import {
    LearningToolSingleton
} from '@learning-tools/learning-tool';
import {
    ProgramMainMenuComponent
} from '../program-main-menu/program-main-menu.component';

@Component({
    selector: 'main-page',
    templateUrl: './main-page.component.html',
    styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

    // Commonality
    @ViewChild('program', {
        static: true,
        read: ViewContainerRef
    }) program: ViewContainerRef;
    @ViewChild('income_main_menu', {
        static: true,
        read: ViewContainerRef
    }) income_main_menu: ViewContainerRef;
    // Variability
    @ViewChild('summary_main_menu', {
        static: true,
        read: ViewContainerRef
    }) summary_main_menu: ViewContainerRef;
    @ViewChild('expense_main_menu', {
        static: true,
        read: ViewContainerRef
    }) expense_main_menu: ViewContainerRef;
    @ViewChild('coa_main_menu', {
        static: true,
        read: ViewContainerRef
    }) coa_main_menu: ViewContainerRef;


    public year: number;
    public isLoggedIn = false;
    public gambar: string;
    public adminRole: any;
    public isAdmin = false;
    public facebook: string;
    public instagram: string;
    public isDrawerOpen = false;
    public isDropdownOpen = false;

    private instance = LearningToolSingleton.getInstance();

    constructor(private comFacResolver: ComponentFactoryResolver, private route: ActivatedRoute, private router: Router, public staticservice: StaticPageService) {

    }

    ngOnInit() {

        this.year = new Date().getFullYear();
        this.staticservice.getstatic().then(data => {
            this.gambar = data["essential_assets"]["logo_url"];
            this.facebook = data["contact"]["url"]["fb_url"];
            this.instagram = data["contact"]["url"]["instagram_url"];
        });
        if (localStorage.getItem('token')) {
            this.isLoggedIn = true;
        } else {
            this.isLoggedIn = false;
        }
        this.adminRole = this.staticservice.adminRole;
        if (this.adminRole.includes(localStorage.getItem('email'))) {
            this.isAdmin = true;
        } else {
            this.isAdmin = false;
        }
    }



    submitLogin() {
        window.location.replace("https://" + "ichlaffterlalu.au.auth0.com" + "/authorize?response_type=id_token&client_id=" + "ED4753GKzpgdvb7sz2pSm9RwwovPTU3q" + "&audience=https://*." + "splelive.id" + "&redirect_uri=" + "http://34.101.241.250" + "/oauth-redirect&scope=openid%20email%20profile&nonce=nduqwihuc78rqwjnAhihniewhfnygr" + "&state=h");
    }

    logout() {
        localStorage.removeItem('token');
        localStorage.removeItem('email');
        location.reload();
    }

    goToAdmin() {
        this.router.navigate(['/admin']);
    }

    setDrawer() {
        this.isDrawerOpen = window.innerWidth <= 720 && !this.isDrawerOpen;
    }

    setDropdown() {
        this.isDropdownOpen = window.innerWidth <= 720 && !this.isDropdownOpen;
    }

    ngAfterViewInit() {
        this.generateDropdown();
    }

    generateDropdown() {
        if (this.instance.getFeatureExplanation().getFeature().getFeatureName() == "program") {
            this.generateProgramMenu();
        } else if (this.instance.getFeatureExplanation().getFeature().getFeatureName() == "catatan-pemasukan") {
            this.generateIncomeMenu();
            this.generateProgramMenu();
        }
    }

    generateProgramMenu() {
        let programFactory = this.comFacResolver.resolveComponentFactory(ProgramMainMenuComponent);
        let programComponent = this.program.createComponent(programFactory);
    }

    generateIncomeMenu() {
        let factory = this.comFacResolver.resolveComponentFactory(IncomeMainMenuComponent);
        let component = this.income_main_menu.createComponent(factory);
    }

}