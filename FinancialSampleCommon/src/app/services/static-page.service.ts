import {
    Injectable
} from '@angular/core';
import {
    environment
} from '../../environments/environment';
import {
    HttpClient,
    HttpHeaders,
    HttpParams
} from '@angular/common/http';


@Injectable({
    providedIn: 'root'
})
export class StaticPageService {



    constructor(private http: HttpClient) {

    }

    httpOptions = {

        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    };

    endpoint: string = environment.rootStaticApi + 'apiadmin/static';
    adminRole = environment.role.administrator

    getstatic() {
        let httpParams = new HttpParams();
        this.httpOptions['params'] = httpParams;
        return this.http.get(this.endpoint, this.httpOptions).toPromise();
    }

    poststatic(data: any) {
        return this.http.post(this.endpoint, data, this.httpOptions).toPromise();
    }
}