export class ChartOfAccount {
    public id: number;
    public name: string;
    public description: string;
    public reference: string;

    constructor(obj ? : any) {
        this.id = obj && obj.id || null;
        this.name = obj && obj.name || null;
        this.description = obj && obj.description || null;
        this.reference = obj && obj.reference || null;
    }


}