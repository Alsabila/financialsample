export class Summary {
    public id: number;
    public datestamp: string;
    public description: string;
    public income: number;
    public expense: number;
    public programName: string;
    public saldo: number;

    constructor(obj ? : any) {
        this.id = obj && obj.id || null;
        this.datestamp = obj && obj.datestamp || null;
        this.description = obj && obj.description || null;
        this.income = obj && obj.income || null;
        this.expense = obj && obj.expense || null;
        this.programName = obj && obj.programName || null;
        this.saldo = obj && obj.saldo || null;
    }


}