export class Confirmation {
    public id: number;
    public accountNumber: string;
    public amount: number;
    public idProgram: number;

    constructor(obj ? : any) {
        this.id = obj && obj.id || null;
        this.accountNumber = obj && obj.accountNumber || null;
        this.amount = obj && obj.amount || null;
        this.idProgram = obj && obj.idProgram || null;
    }


}