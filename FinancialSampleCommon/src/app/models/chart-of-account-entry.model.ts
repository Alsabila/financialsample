export class ChartOfAccountEntry {
    public idChartOfAccount: number;
    public amount: number;
    public level: number;
    public name: string;
    public description: string;

    constructor(obj ? : any) {
        this.idChartOfAccount = obj && obj.idChartOfAccount || null;
        this.amount = obj && obj.amount || null;
        this.level = obj && obj.level || null;
        this.name = obj && obj.name || null;
        this.description = obj && obj.description || null;
    }


}