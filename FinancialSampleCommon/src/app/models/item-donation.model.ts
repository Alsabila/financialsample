export class ItemDonation {
    public id: number;
    public itemName: string;
    public quantity: number;
    public idProgram: number;

    constructor(obj ? : any) {
        this.id = obj && obj.id || null;
        this.itemName = obj && obj.itemName || null;
        this.quantity = obj && obj.quantity || null;
        this.idProgram = obj && obj.idProgram || null;
    }


}