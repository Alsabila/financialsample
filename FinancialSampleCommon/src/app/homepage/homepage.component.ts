import {
    Component,
    OnInit
} from '@angular/core';
import {
    StaticPageService
} from '../services/static-page.service';

import * as introJs from 'intro.js/intro.js';
import {
    LearningToolSingleton
} from '@learning-tools/learning-tool';

@Component({
    selector: 'app-homepage',
    templateUrl: './homepage.component.html',
    styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

    public detail: string;
    public address: string;
    public map: string;
    public email: string;
    public phone: string;
    public logoUrl: string;
    public banner: string;
    public aboutUs: string;
    public slogan: string;
    public bgBanner: string;

    instance = LearningToolSingleton.getInstance();
    introFeature = introJs();

    constructor(public staticservice: StaticPageService) {
        if (localStorage.getItem("Learning Tool Status") == "Done") {
            this.instance.setShowIntroJS(false);
        } else {
            this.setLearningTool()
        }
    }

    setLearningTool() {
        this.instance.getFinancialReminderAlert();
        this.introFeature.setOptions(this.instance.getExplanationTooltip());
        this.introFeature.setOptions(this.instance.getFeatureExplanationIntroJSText());
    }

    ngOnInit() {
        this.staticservice.getstatic().then(data => {
            this.detail = data["page"]["description"];
            this.aboutUs = data["page"]["about_us"];
            this.slogan = data["page"]["slogan"];
            this.address = data["contact"]["address"];
            this.map = data["contact"]["google_map_url"];
            this.email = data["contact"]["email"];
            this.phone = data["contact"]["phone"];
            this.logoUrl = data["essential_assets"]["logo_url"];
            this.banner = data["essential_assets"]["banner_url"];
            this.bgBanner = 'linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)),url("' + this.banner + '")';
        });
        if (this.instance.showIntroJS()) {
            this.introFeature.start();
        }
    }

}