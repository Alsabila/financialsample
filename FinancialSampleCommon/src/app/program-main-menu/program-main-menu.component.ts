import {
    Component,
    OnInit
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';
import {
    ApiProgramListAbsService
} from '../services/api-program-list-abs.service';

import * as introJs from 'intro.js/intro.js';
import {
    LearningToolSingleton
} from '@learning-tools/learning-tool';

@Component({
    selector: 'program-main-menu',
    templateUrl: './program-main-menu.component.html',
    styleUrls: ['./program-main-menu.component.css']
})
export class ProgramMainMenuComponent implements OnInit {


    instance = LearningToolSingleton.getInstance()
    introFeature = introJs();

    constructor(private route: ActivatedRoute, private router: Router, public apiprogramlistabsservice: ApiProgramListAbsService) {
        if (localStorage.getItem("Learning Tool Status") == "Done") {
            this.instance.setShowIntroJS(false);
            this.instance.setShowLearningTools(false);
        } else {
            this.setLearningTool()
        }
    }

    setLearningTool() {
        this.introFeature.setOptions(this.instance.getExplanationTooltip());
        this.introFeature.setOptions(this.instance.getFeatureExplanationIntroJSText());

        if (this.instance.getFeatureExplanationStatus() == "Finished") {
            this.instance.writeExplanationInLocalStorage();
            this.introFeature.oncomplete(() => {
                this.instance.setShowIntroJS(false);
                this.instance.getAssessmentResult();
            })
        }
    }

    ngOnInit() {
        if (this.instance.showIntroJS()) {
            this.introFeature.start();
        }

    }

    program() {
        this.apiprogramlistabsservice.call().then(data => {
            this.router.navigate(['/daftar-program'], {
                queryParams: {
                    jsonAllProgram: JSON.stringify(data['data'])
                }
            });
        }).catch(error => {
            this.router.navigate(['/daftar-program'], {
                queryParams: {
                    jsonAllProgram: JSON.stringify([])
                }
            });
        });
    }

}