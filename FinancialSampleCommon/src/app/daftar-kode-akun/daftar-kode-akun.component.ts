import {
    Component,
    OnInit,
    Input
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';
import {
    ChartOfAccount
} from '../models/chart-of-account.model';

@Component({
    selector: '[daftar-kode-akun]',
    templateUrl: './daftar-kode-akun.component.html',
    styleUrls: ['./daftar-kode-akun.component.css']
})
export class DaftarKodeAkunComponent implements OnInit {

    @Input() jsonAllCoa: any;
    public coaElement: ChartOfAccount;

    constructor(private route: ActivatedRoute, private router: Router) {

    }

    ngOnInit() {
        this.attachcoaelement();
    }

    currencyFormatDE(num) {
        if (num == null) {
            return "0,00"
        } else {
            return (
                num
                .toFixed(2) // always two decimal digits
                .replace('.', ',') // replace decimal point character with ,
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
            ) // use . as a separator
        }
    }

    attachcoaelement() {
        this.coaElement = new ChartOfAccount(this.jsonAllCoa);
    }

}