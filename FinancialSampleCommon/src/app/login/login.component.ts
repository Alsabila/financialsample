import {
    Component,
    OnInit
} from '@angular/core';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    constructor() {}

    public isLoggedIn = false;
    public profileImage = "";

    ngOnInit() {
        if (localStorage.getItem('token')) {
            this.isLoggedIn = true;
        } else {
            this.isLoggedIn = false;
        }
    }

    submitLogin() {
        window.location.replace("https://" + "ichlaffterlalu.au.auth0.com" + "/authorize?response_type=id_token&client_id=" + "ED4753GKzpgdvb7sz2pSm9RwwovPTU3q" + "&audience=https://*." + "splelive.id" + "&redirect_uri=" + "http://34.101.241.250" + "/oauth-redirect&scope=openid%20email%20profile&nonce=nduqwihuc78rqwjnAhihniewhfnygr" + "&state=h");
    }

    public logout() {
        localStorage.removeItem('token');
        localStorage.removeItem('email');
        location.reload();
    }

}