import {
    Component,
    OnInit
} from '@angular/core';
import {
    ActivatedRoute,
    Router
} from '@angular/router';
import {
    ApiProgramListAbsService
} from '../services/api-program-list-abs.service';
import {
    ApiChartOfAccountListAbsService
} from '../services/api-chart-of-account-list-abs.service';
import {
    ApiIncomeSaveAbsService
} from '../services/api-income-save-abs.service';

import * as introJs from 'intro.js/intro.js';
import {
    LearningToolSingleton
} from '@learning-tools/learning-tool';

@Component({
    selector: 'tambahkan-pemasukan',
    templateUrl: './tambahkan-pemasukan.component.html',
    styleUrls: ['./tambahkan-pemasukan.component.css']
})
export class TambahkanPemasukanComponent implements OnInit {

    public tanggal: Date;
    public deskripsi: string;
    public jumlah: number;
    public namaProgramTerkait: string;
    public programOptions: any;
    public kodeAkun: string;
    public options: any;
    instance = LearningToolSingleton.getInstance()
    introFeature = introJs();

    constructor(private route: ActivatedRoute, private router: Router, public apiprogramlistabsservice: ApiProgramListAbsService, public apichartofaccountlistabsservice: ApiChartOfAccountListAbsService, public apiincomesaveabsservice: ApiIncomeSaveAbsService) {
        if (localStorage.getItem("Learning Tool Status") == "Done") {
            this.instance.setShowIntroJS(false);
            this.instance.setShowLearningTools(false);
        } else {
            this.setLearningTool()
        }
    }

    setLearningTool() {
        this.introFeature.setOptions(this.instance.getExplanationTooltip());
        this.introFeature.setOptions(this.instance.getFeatureExplanationIntroJSText());

        if (this.instance.getFeatureExplanationStatus() == "Finished") {
            this.instance.writeExplanationInLocalStorage();
            this.introFeature.oncomplete(() => {
                this.instance.setShowIntroJS(false);
                this.instance.getAssessmentResult();
            })
        }
    }

    ngOnInit() {
        if (this.instance.showIntroJS()) {
            this.introFeature.start();
        }
        this.getProgram();
        this.getCoa();
    }

    async getProgram() {
        await this.apiprogramlistabsservice.call().then(data => {
            this.programOptions = data['data'];
        });
    }
    async getCoa() {
        await this.apichartofaccountlistabsservice.call().then(data => {
            this.options = data['data'];
        });
    }
    kirim() {
        this.apiincomesaveabsservice.call({
            datestamp: this.tanggal,
            description: this.deskripsi,
            amount: this.jumlah,
            programName: this.namaProgramTerkait,
            idCoa: this.kodeAkun
        }).then(data => {
            this.instance.updateAssessmentData("Income", data['data'].length);
            if (localStorage.getItem("Explanation Status") == "Finished") {
                this.instance.getAssessmentResult();
            }
            this.router.navigate(['/catatan-pemasukan'], {
                queryParams: {
                    jsonAllIncome: JSON.stringify(data['data'])
                }
            });
        }).catch(error => {
            this.router.navigate(['/catatan-pemasukan'], {
                queryParams: {
                    jsonAllIncome: JSON.stringify([])
                }
            });
        });
    }

}