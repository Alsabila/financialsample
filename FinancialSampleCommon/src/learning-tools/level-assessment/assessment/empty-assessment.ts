import {
    Assessment
} from "./Assessment";
import {
    NextFeature
} from "../assessment-object/AssessmentObject";

export class EmptyAssessment extends Assessment {

    private nextFeatureEmpty = new NextFeature("", "", "");

    constructor() {
        super();
    }

    modifyNextFeature(nextFeatureName: string) {
        this.setNextFetures(this.nextFeatureEmpty);
    }

    updateData(name: string, count: number) {
        console.log("Learning Tool already finished");
    }

    getStatus() {
        return "Achieved";
    }

    getAssessmentName() {
        return "empty";
    }

}