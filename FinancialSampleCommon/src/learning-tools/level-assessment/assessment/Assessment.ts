import {
    Requirement,
    NextFeature
} from "../assessment-object/AssessmentObject";

export abstract class Assessment implements AssessmentInterface {
    private requirements: Array < Requirement > ;
    private nextFeat: NextFeature;

    public getRequirements() {
        return this.requirements;
    }
    public getNextFeature() {
        return this.nextFeat;
    }

    public getHtmlTemplateRequirements() {
        let template = "";
        this.requirements.forEach(element => {
            template += element.getHtmlTemplate();
        });
        return template;
    }

    public getHtmlTemplateNextFeature() {
        let template = this.nextFeat.getHtmlTemplate();
        return template;
    }

    public setRequirements(arr: Array < Requirement > ) {
        this.requirements = arr;
    }

    public setNextFetures(nextF: NextFeature) {
        this.nextFeat = nextF;
    }

    abstract updateData(name: string, count: number)
    abstract getStatus()
    abstract getAssessmentName();
    abstract modifyNextFeature(nextFeatureName: string);
}