import {
    Requirement,
    NextFeature
} from "../assessment-object/AssessmentObject";
import {
    Assessment
} from "./Assessment";

export class AssessmentTwo extends Assessment {
    private req1 = new Requirement("Jumlah program yang sedang/telah berlangsung mencapai 3 program", 2, 0);
    private req2 = new Requirement("Jumlah pemasukan mencapai 100 transaksi", 2, 0);

    private nextFeatureExpense = new NextFeature("Catatan Pengeluaran", "Menyajikan catatan pengeluaran dana untuk situs Anda",
        require('../../../assets/images/expense.png'));
    private nextFeatureSummary = new NextFeature("Catatan Transaksi", "Menyajikan catatan transaksi keuangan organisasi Anda",
        require('../../../assets/images/manual.png'));
    private nextFeatureReport = new NextFeature("Laporan Keuangan", "Membuat laporan otomatis sesuai format PSAK-45",
        require('../../../assets/images/automatic_45.png'));
    private nextFeatureEmpty = new NextFeature("", "", "");

    constructor() {
        super();
        this.updateData("All", 0);
        this.setRequirements([this.req1, this.req2]);
        this.setNextFetures(this.nextFeatureExpense);
    }

    modifyNextFeature(nextFeatureName: string) {
        if (nextFeatureName == "catatan-transaksi") this.setNextFetures(this.nextFeatureSummary);
        else if (nextFeatureName == "laporan-keuangan") this.setNextFetures(this.nextFeatureReport);
        else if (nextFeatureName == "empty") this.setNextFetures(this.nextFeatureEmpty);
        else console.log("Already constructed");
    }

    getProgramCount() {
        fetch('http://34.101.241.250/api/program/list.abs').then(res => res.json())
            .then(data => this.req1.setCurrentTotal(data['data'].length))
            .catch(error => console.log("Error"));
    }

    getIncomeCount() {
        fetch('http://34.101.241.250/api/income/list.abs').then(res => res.json())
            .then(data => this.req2.setCurrentTotal(data['data'].length))
            .catch(error => console.log("Error"));

    }

    updateData(name: string, count: number) {
        if (name == "All") {
            this.getProgramCount();
            this.getIncomeCount();
        } else if (name == "Program") {
            this.req1.setCurrentTotal(count);
        } else if (name == "Income") {
            this.req2.setCurrentTotal(count);
        }
    }

    getStatus() {
        if (this.req1.getCurrentTotal() >= this.req1.getTotal() &&
            this.req2.getCurrentTotal() >= this.req2.getTotal()) {
            return "Achieved";
        } else {
            return "Not Achieved";
        }
    }

    getAssessmentName() {
        return "catatan-pemasukan";
    }
}