import {
    EmptyStep
} from "./step/empty-step";
import {
    HomepageStep,
    AddProgramStep,
    FormProgramStep,
    FinishProgramStep
} from "./step/program-step";

export class FeatureProgram implements Feature {
    private empty: EmptyStep;
    private homepage: HomepageStep;
    private addProgram: AddProgramStep;
    private formProgram: FormProgramStep;
    private finishProgram: FinishProgramStep;

    private currentStep: Step;
    private currentStatus: string;

    constructor() {
        this.empty = new EmptyStep();

        this.homepage = new HomepageStep();
        this.addProgram = new AddProgramStep();
        this.formProgram = new FormProgramStep();
        this.finishProgram = new FinishProgramStep();

        this.setStep(this.homepage);
        this.setStatus("Not Finished");
    }

    getStatus() {
        return this.currentStatus;
    }

    setStatus(status: string) {
        this.currentStatus = status;
    }

    getStep() {
        if (this.currentStatus == "Not Finished") {
            if (window.location.pathname == '/homepage' && this.currentStep instanceof HomepageStep) {
                // do nothing
            } else if (window.location.pathname == '/daftar-program' && this.currentStep instanceof HomepageStep) {
                this.setStep(this.addProgram);
            } else if (window.location.pathname == '/daftar-program' && this.currentStep instanceof FormProgramStep) {
                this.setStep(this.finishProgram);
                this.setStatus("Finished");
            } else if (window.location.pathname == '/halaman-tambah-program') {
                this.setStep(this.formProgram);
            } else {
                this.setStep(this.empty);
            }
        } else {
            this.setStep(this.empty);
        }
        return this.currentStep;
    }

    setStep(step: Step) {
        this.currentStep = step;
    }

    getFeatureName() {
        return "program";
    }

}