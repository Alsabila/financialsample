import * as introJs from 'intro.js/intro.js';

export class HomepageStep implements Step {
    getExplanation() {
        return {
            doneLabel: "Ok, saya klik Program!",
            steps: [{
                intro: "Selamat datang di beranda halaman Anda. Anda akan dipandu untuk menggunakan sistem informasi buatan AISCO. Mohon ikuti semua arahan kami. Selamat belajar :)"
            }, {
                intro: "Anda akan diarahkan untuk membuat Program baru"
            }, {
                element: '#button-fitur',
                intro: "Klik menu Fitur pada menu bar lalu pilih fitur Program"
            }]
        }
    }
}

export class AddProgramStep implements Step {
    getExplanation() {
        return {
            doneLabel: "Ok, saya klik Tambah Program!",
            steps: [{
                intro: "Selamat datang di halaman Program"
            }, {
                element: '#v-menu-tambah-program',
                intro: "Tekan tombol 'Tambah Program' untuk menambah program baru",
                disableInteraction: true
            }]
        }
    }
}

export class FormProgramStep implements Step {
    getExplanation() {
        return {
            doneLabel: "Ok, saya buat program baru!",
            steps: [{
                intro: "Isi formulir ini dengan informasi mengenai program baru Anda",
            }, {
                element: '#input-nama-program',
                intro: "Masukkan nama program baru Anda. Contoh: My New Program"
            }, {
                element: '#input-deskripsi',
                intro: "Masukkan deskripsi mengenai program baru Anda. Contoh: Ini Program baru"
            }, {
                element: '#input-target',
                intro: "Masukkan jumlah target pencapaian program baru Anda. Contoh: 100"
            }, {
                element: '#input-partner',
                intro: "Masukkan nama partner program baru Anda. Contoh: My Partner"
            }, {
                element: '#div-url-gambar-program',
                intro: "Masukkan gambar program baru Anda. Bila tidak ada gambar, boleh dilewati"
            }, {
                element: '#onsubmit-event-kirim',
                disableInteraction: true,
                intro: "Setelah formulir dilengkapi, tekan tombol ini untuk mengirim formulir"
            }]
        }
    }
}

export class FinishProgramStep implements Step {
    getExplanation() {
        return {
            doneLabel: "Oke, saya berhasil!",
            steps: [{
                intro: "Selamat! Anda berhasil membuat program baru!"
            }]
        }
    }
}