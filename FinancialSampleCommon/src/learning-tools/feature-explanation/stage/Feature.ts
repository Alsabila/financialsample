interface Feature {
    getStep();
    setStep(step: Step);
    getStatus();
    setStatus(status: string);
    getFeatureName();
}