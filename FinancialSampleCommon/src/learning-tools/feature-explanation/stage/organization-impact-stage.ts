import {
    EmptyStep
} from "./step/empty-step";
import {
    StartExpenseStep,
    AddExpenseStep,
    FormExpenseStep,
    FinishExpenseStep
} from "./step/expense-step";

export class FeatureExpense implements Feature {
    private empty: EmptyStep;
    private startExpense: StartExpenseStep;
    private addExpense: AddExpenseStep;
    private formExpense: FormExpenseStep;
    private finishExpense: FinishExpenseStep;

    private currentStep: Step;
    private currentStatus: string;

    constructor() {
        this.empty = new EmptyStep();
        this.startExpense = new StartExpenseStep();
        this.addExpense = new AddExpenseStep();
        this.formExpense = new FormExpenseStep();
        this.finishExpense = new FinishExpenseStep();

        this.setStep(this.startExpense);
        this.setStatus("Not Finished");
    }

    getFeatureName() {
        return "catatan-pengeluaran";
    }

    getStep() {
        if (this.currentStatus == "Not Finished") {
            if (window.location.pathname == '/homepage' && this.currentStep instanceof StartExpenseStep) {
                // do nothing
            } else if (window.location.pathname == '/catatan-pengeluaran' && this.currentStep instanceof StartExpenseStep) {
                this.setStep(this.addExpense);
            } else if (window.location.pathname == '/catatan-pengeluaran' && this.currentStep instanceof FormExpenseStep) {
                this.setStep(this.finishExpense);
                this.setStatus("Finished");
            } else if (window.location.pathname == '/halaman-tambah-pengeluaran') {
                this.setStep(this.formExpense);
            } else {
                this.setStep(this.empty);
            }
        } else {
            this.setStep(this.empty);
        }
        return this.currentStep;
    }

    setStep(step: Step) {
        this.currentStep = step;
    }

    getStatus() {
        return this.currentStatus;
    }

    setStatus(status: string) {
        this.currentStatus = status;
    }

}