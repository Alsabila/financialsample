export class FeatureExplanation {
    public currentfeature: Feature;

    constructor(feature: Feature) {
        this.setFeature(feature);
    }

    public setFeature(feature: Feature) {
        this.currentfeature = feature;
    }

    public getFeature() {
        return this.currentfeature;
    }

}